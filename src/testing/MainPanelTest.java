package testing;

import javax.swing.JPanel;

public class MainPanelTest extends JPanel{

	public MainPanelTest() {
		setLayout(null);
		
		add(new ButtonPanel());
		add(new BackgroundPanel());
	}
}
