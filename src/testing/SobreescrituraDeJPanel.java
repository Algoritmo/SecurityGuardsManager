package testing;

import javax.swing.JFrame;
import javax.swing.JPanel;

import guardsPanel.GuardsButtonPanel;
import mainMenu.MainMenuButtonPanel;

public class SobreescrituraDeJPanel extends JFrame {
	
	public static void main(String[] args) {
		(new SobreescrituraDeJPanel()).setVisible(true);
	}
	
	public SobreescrituraDeJPanel() {
		setUndecorated(true);
		setSize(650,500);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JPanel botones =  new GuardsButtonPanel(); 
		add( botones );
		remove( botones );
		botones =new MainMenuButtonPanel();
		add(botones);
		botones.repaint();
	}
}
