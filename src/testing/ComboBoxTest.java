package testing;

import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;

public class ComboBoxTest extends JPanel {

	/**
	 * Create the panel.
	 */
	public ComboBoxTest() {
		setLayout(null);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerListModel(new String[] {"VIP", "Alto", "Medio", "Bajo"}));
		spinner.setBounds(10, 11, 65, 18);
		add(spinner);

	}
}
