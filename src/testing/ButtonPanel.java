package testing;

import javax.swing.JPanel;

import sharedClasses.SharedComponents;

public class ButtonPanel extends JPanel{

	public ButtonPanel() {
		setLayout(null);
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		
		add(SharedComponents.btnExit());
		add(SharedComponents.btnLogOut());
		add(SharedComponents.btnMainMenu());
	}
}
