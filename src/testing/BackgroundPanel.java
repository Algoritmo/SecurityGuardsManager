package testing;

import javax.swing.JPanel;

import sharedClasses.SharedComponents;

public class BackgroundPanel extends JPanel {

	public BackgroundPanel() {
		setLayout(null);
		setBounds(0, 0, 650, 500);
		add(SharedComponents.lblSessionBackground());
	}
}
