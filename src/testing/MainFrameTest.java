package testing;

import javax.swing.JFrame;

import guardsPanel.ListGuardsWorkPanel;

public class MainFrameTest extends JFrame {
	
	public static void main(String[] args) {
		(new MainFrameTest()).setVisible(true);
	}
	
	public MainFrameTest () {
		setUndecorated(true);
		setSize(650, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(new ListGuardsWorkPanel());
	}
}
