package testing;

import javax.swing.JButton;
import javax.swing.JPanel;

import sharedClasses.DefaultComponentsConfiguration;
import sharedClasses.SharedComponents;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;

public class AddGuardsPhoneMartialArtsWorkPanel extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	
	public AddGuardsPhoneMartialArtsWorkPanel() {
		setSize(650, 500);
		setLayout(null);

		JButton algo = new JButton("algo");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(algo);
		algo.setBounds(20, 140, 132, 23);
		
		add(algo);
		add( SharedComponents.btnExit());
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono 1:");
		lblTelfono.setBounds(211, 144, 70, 14);
		add(lblTelfono);
		
		JLabel lblTelfono_1 = new JLabel("Tel\u00E9fono 2:");
		lblTelfono_1.setBounds(337, 144, 70, 14);
		add(lblTelfono_1);
		
		JLabel lblTelfono_2 = new JLabel("Tel\u00E9fono 3:");
		lblTelfono_2.setBounds(464, 144, 70, 14);
		add(lblTelfono_2);
		
		textField = new JTextField();
		textField.setBounds(211, 169, 109, 20);
		add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(337, 169, 109, 20);
		add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(464, 169, 109, 20);
		add(textField_2);
		
		JLabel lblArteMarcialPrincipal = new JLabel("Arte marcial principal:");
		lblArteMarcialPrincipal.setBounds(211, 217, 132, 14);
		add(lblArteMarcialPrincipal);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerListModel(new String[] {"Aikido", "Boxeo", "Boxeo frances", "Capoeira", "Defensa personal", "Esgrima", "Iaido", "Jeet kune do", "Jiu jitsu", "Judo", "Kali escrima", "Karate", "Kendo", "Kick boxing", "Kobudo", "Kung fu", "Muay thai", "Ninjutsu", "Pencak silat", "Tai chi", "Taijutsu", "Taekwondo", "Wing Tsun"}));
		spinner.setBounds(430, 217, 143, 18);
		add(spinner);
		
		JLabel lblOtrasArtesMarciales = new JLabel("Artes marciales secundarias:");
		lblOtrasArtesMarciales.setBounds(211, 244, 148, 14);
		add(lblOtrasArtesMarciales);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBounds(430, 244, 143, 18);
		add(spinner_1);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setBounds(430, 273, 143, 18);
		add(spinner_2);
		
		JSpinner spinner_3 = new JSpinner();
		spinner_3.setBounds(430, 298, 143, 18);
		add(spinner_3);
		
		JSpinner spinner_4 = new JSpinner();
		spinner_4.setBounds(430, 325, 143, 18);
		add(spinner_4);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(430, 378, 143, 23);
		add(btnGuardar);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(331, 378, 89, 23);
		add(btnVolver);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.setBounds(243, 378, 89, 23);
		add(btnLimpiar);
	}
}
