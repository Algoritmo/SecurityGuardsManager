package clientsButtonPanel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import sharedClasses.DefaultComponentsConfiguration;

public class DeleteClientWorkPanel extends JPanel {
	private static JLabel lblInputClientCode;
	private static JTextField txtFInputCode;
	private static JButton btnRemove;
	
	static {
		lblInputClientCode = new JLabel("Ingrese el c�digo de cliente: ");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputClientCode);
		lblInputClientCode.setBounds(191, 144, 250, 14);
		lblInputClientCode.setFont(new Font("Tahoma", Font.BOLD, 12));

		txtFInputCode = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFInputCode);
		txtFInputCode.setBounds(443, 141, 132, 19);
		
		btnRemove = new JButton("Borrar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnRemove);
		btnRemove.setBounds(443, 165, 132, 23);
	}
	
	public DeleteClientWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		add( lblInputClientCode );
		add( txtFInputCode );
		add( btnRemove );
	}
}
