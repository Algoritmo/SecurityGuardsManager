package clientsButtonPanel;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JPanel;

import sharedClasses.DefaultComponentsConfiguration;
import sharedClasses.SharedComponents;

public class ClientsButtonPanel extends JPanel {
	private static JButton btnSearchCustomer, btnEditCustomer, btnAddCustomer, 
								btnRemoveCustomer, btnListCustomers,
								btnMainMenu = SharedComponents.btnMainMenu();
	
	static {
		btnSearchCustomer = new JButton("Buscar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnSearchCustomer);
		btnSearchCustomer.setBounds(20, 140, 132, 23);
		btnSearchCustomer.addActionListener(new ClientsListener());
			
		
		btnEditCustomer = new JButton("Editar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnEditCustomer);
		btnEditCustomer.setBounds(20, 170, 132, 23);
		btnEditCustomer.addActionListener(new ClientsListener());

		btnAddCustomer = new JButton("A�adir cliente");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnAddCustomer);	
		btnAddCustomer.setBounds(20, 200, 132, 23);
		btnAddCustomer.addActionListener(new ClientsListener());
			
		btnRemoveCustomer = new JButton("Borrar cliente");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnRemoveCustomer);
		btnRemoveCustomer.setBounds(20, 230, 132, 23);
		btnRemoveCustomer.addActionListener(new ClientsListener());
		
		btnListCustomers = new JButton("Listar clientes");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnListCustomers);
		btnListCustomers.setBounds(20, 260, 132, 23);
		btnListCustomers.addActionListener(new ClientsListener());
	}
	
	
	public ClientsButtonPanel() {
		setLayout(null);
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		
		add( btnAddCustomer );
		add( btnEditCustomer );
		add( btnListCustomers );
		add( btnRemoveCustomer );
		add( btnSearchCustomer );
		
		add( btnMainMenu );
	}
}
