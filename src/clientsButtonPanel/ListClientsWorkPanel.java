package clientsButtonPanel;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class ListClientsWorkPanel extends JPanel{
	public static JScrollPane scrollPaneClientsInfo ; 
	
	static {
		scrollPaneClientsInfo = new JScrollPane();
		scrollPaneClientsInfo.setHorizontalScrollBarPolicy(
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPaneClientsInfo.setVerticalScrollBarPolicy(
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPaneClientsInfo.setBounds(160, 140, 463, 343);
			
		JTextArea txtAreaClientsInfo = new JTextArea();
		txtAreaClientsInfo.setForeground(new Color(255, 153, 0));
		txtAreaClientsInfo.setBackground(Color.BLACK);
		txtAreaClientsInfo.setEditable(false);
			
		scrollPaneClientsInfo.setViewportView(txtAreaClientsInfo);
	}
	
	public ListClientsWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		add( scrollPaneClientsInfo );
	}
}
