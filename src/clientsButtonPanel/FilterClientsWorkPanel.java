package clientsButtonPanel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;

import sharedClasses.DefaultComponentsConfiguration;

public class FilterClientsWorkPanel extends JPanel {
	public static JLabel lblKindOfService;
	public static JSpinner spnnrCategory;
	public static JButton btnFilter;
	public static JRadioButton rdBtnSearch, rdBtnFilter;
	
	static {
		rdBtnSearch = new JRadioButton("Buscar");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnSearch);
		rdBtnSearch.setFont(new Font("Tahoma", Font.BOLD, 14));
		rdBtnSearch.setBounds(178, 139, 97, 25);
		rdBtnSearch.addActionListener( new RdBtnSearchFilterClientListener ());
			
		rdBtnFilter = new JRadioButton("Filtrar");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnFilter);
		rdBtnFilter.setSelected(true);
		rdBtnFilter.setFont(new Font("Tahoma", Font.BOLD, 14));
		rdBtnFilter.setBounds(292, 140, 97, 25);
		rdBtnFilter.addActionListener(new RdBtnSearchFilterClientListener());
			
		
		lblKindOfService = new JLabel("Tipo de servicio contratado:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblKindOfService);
		lblKindOfService.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblKindOfService.setBounds(178, 189, 201, 25);
			
		spnnrCategory = new JSpinner();
		spnnrCategory.setModel(new SpinnerListModel(new String[] {"VIP", "Alto", "Medio", "Bajo"}));
		spnnrCategory.setBounds(387, 190, 136, 25);
			
		
		btnFilter = new JButton("Filtrar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnFilter);
		btnFilter.setBounds(387, 236, 136, 23);
	}
	
	public FilterClientsWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		add( rdBtnFilter );
		add( rdBtnSearch );
		
		add( lblKindOfService );
		add( spnnrCategory );
		
		add( btnFilter );
	}
}
