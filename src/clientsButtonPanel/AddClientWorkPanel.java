package clientsButtonPanel;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;

import sharedClasses.DefaultComponentsConfiguration;

public class AddClientWorkPanel extends JPanel {
	private static JLabel lblCIInputInfo, lblCode, lblDesignatedGuards, lblHiredService,
						lblInputInfo, lblInputNewData, lblName;
	private static JSpinner spnnrKindOfService;
	private static JTextField txtFCode, txtFName;
	private static JScrollPane jscrllInputGuardsCI;
	private static JButton btnSave;
	
	static {
		lblName = new JLabel("Nombre:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblName);
		lblName.setBounds(180, 199, 82, 14);
		
	
		lblInputNewData = new JLabel("Ingrese datos nuevos:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputNewData);
		lblInputNewData.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblInputNewData.setBounds(180, 138, 162, 22);
		lblInputNewData.setOpaque(true);
		
		lblInputInfo = new JLabel("(No debe dejar casillas en blanco)");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputInfo);
		lblInputInfo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblInputInfo.setBounds(180, 158, 278, 20);
	
		lblCode = new JLabel("C\u00F3digo:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblCode);
		lblCode.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCode.setBounds(180, 224, 82, 14);
	
		lblHiredService = new JLabel("Servicio contratado:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblHiredService);
		lblHiredService.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblHiredService.setBounds(180, 249, 127, 14);
	
		lblDesignatedGuards = new JLabel("Guardias designados:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblDesignatedGuards);
		lblDesignatedGuards.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDesignatedGuards.setBounds(180, 274, 127, 14);
		
	
		lblCIInputInfo = new JLabel("(S\u00F3lo ingrese CI separadas por coma y espacio \", \")");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblCIInputInfo);
		lblCIInputInfo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblCIInputInfo.setBounds(180, 288, 255, 20);
		
		/*
		 ** j textField **
		 */
		txtFName = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFName);
		txtFName.setBounds(438, 199, 144, 20);
	
		txtFCode = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFCode);
		txtFCode.setBounds(438, 224, 144, 20);
		
		/*
		 ** spinner **
		 */
		spnnrKindOfService = new JSpinner();
		spnnrKindOfService.setModel(
				new SpinnerListModel(new String[] {"VIP", "Alto", "Medio", "Bajo"}));
		spnnrKindOfService.setBounds(438, 250, 144, 18);
		
		
		jscrllInputGuardsCI = new JScrollPane();
		jscrllInputGuardsCI.setVerticalScrollBarPolicy(
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		jscrllInputGuardsCI.setHorizontalScrollBarPolicy(
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jscrllInputGuardsCI.setBounds(438, 279, 162, 52);
		
		JTextArea textArea = new JTextArea();
		textArea.setWrapStyleWord(true);
		textArea.setBackground(Color.BLACK);
		textArea.setForeground(new Color(255, 153, 0));
		jscrllInputGuardsCI.setViewportView(textArea);
		
		/*
		 ** j button **
		 */
		btnSave = new JButton("Guardar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnSave);
		btnSave.setBounds(438, 356, 144, 23);
	}

	public AddClientWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		add( lblCIInputInfo );
		add( lblCode );
		add( lblDesignatedGuards );
		add( lblHiredService );
		add( lblInputInfo );
		add( lblInputNewData );
		add( lblName );
		
		add( spnnrKindOfService );
		
		add( txtFCode );
		add( txtFName );
		
		add( jscrllInputGuardsCI );
		
		add( btnSave );
	}
}
