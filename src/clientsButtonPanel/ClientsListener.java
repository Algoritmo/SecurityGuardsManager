package clientsButtonPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import adminWindows.AdminWindow;
import mainWindow.MainWindow;
import sharedClasses.SharedMethods;

public class ClientsListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
String fontOfTheEvent = e.getActionCommand();
		
		MainWindow.mainFrame.remove(AdminWindow.workPanel);
		
		if (fontOfTheEvent.equals("Listar clientes")) 
			AdminWindow.workPanel = new ListClientsWorkPanel();
		else if (fontOfTheEvent.equals("Editar"))
			AdminWindow.workPanel = new EditClientWorkPanel();
		else if (fontOfTheEvent.equals("Borrar cliente"))
			 AdminWindow.workPanel = new DeleteClientWorkPanel();
		else if (fontOfTheEvent.equals("A�adir cliente"))
			 AdminWindow.workPanel = new AddClientWorkPanel();
		else if (fontOfTheEvent.equals("Buscar"))
			 AdminWindow.workPanel = new SearchClientWorkPanel();
			 
		MainWindow.mainFrame.add(AdminWindow.workPanel);
		SharedMethods.refreshPanels();
	}
}
