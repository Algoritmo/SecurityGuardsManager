package clientsButtonPanel;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;

import sharedClasses.DefaultComponentsConfiguration;

public class EditClientWorkPanel extends JPanel{
	private static JLabel lblInputClientsCode, lblName, lblDesignedGuards, lblHiredService,
								lblCIInputInfo, lblInputNewData, lblGeneralInputInfo; 
	private static JTextField txtFClientsCode, txtFClientsName;
	private static JSpinner spnnrKindOfService;
	private static JScrollPane scrllPaneDesignedGuards;
	private static JButton btnApplyChanges;
	private static JTextArea textArea;
	
	static {
		/*
		 ** j label **
		 */
		lblInputClientsCode = new JLabel("Ingrese el c\u00F3digo del cliente a modificar:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputClientsCode);
		lblInputClientsCode.setBounds(173, 139, 258, 22);
			
		lblName = new JLabel("Nombre:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblName);
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblName.setBounds(176, 231, 82, 14);
			
		lblHiredService = new JLabel("Servicio contratado:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblHiredService);
		lblHiredService.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblHiredService.setBounds(176, 256, 127, 14);

		lblDesignedGuards = new JLabel("Guardias designados:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblDesignedGuards);
		lblDesignedGuards.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDesignedGuards.setBounds(176, 281, 127, 14);
			
		lblCIInputInfo = new JLabel(
				"(S\u00F3lo ingrese CI separadas por coma y espacio \", \")");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblCIInputInfo);
		lblCIInputInfo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblCIInputInfo.setBounds(176, 295, 255, 20);
			
		lblInputNewData = new JLabel("Ingrese los nuevos datos:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputNewData);
		lblInputNewData.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblInputNewData.setBounds(173, 185, 172, 22);
		
		lblGeneralInputInfo = new JLabel(
				"(Si hay un dato que no quiera modificar, d\u00E9jelo en blanco)");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblGeneralInputInfo);
		lblGeneralInputInfo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblGeneralInputInfo.setBounds(173, 205, 301, 20);
		
		/*
		 ** j text field **
		 */
		txtFClientsCode = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFClientsCode);
		txtFClientsCode.setBounds(472, 141, 144, 20);
			
		txtFClientsName  = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFClientsName);
		txtFClientsName.setBounds(472, 231, 144, 20);
		
		/*
		 ** j spinner **
		 */
		spnnrKindOfService = new JSpinner();
		spnnrKindOfService.setModel(new SpinnerListModel(new String[] {"VIP", "Alto", "Medio", "Bajo"}));
		spnnrKindOfService.setForeground(Color.ORANGE);
		spnnrKindOfService.setBackground(Color.BLACK);
		spnnrKindOfService.setBounds(472, 257, 144, 18);
		
		/*
		 ** j scroll pane **
		 */
		scrllPaneDesignedGuards = new JScrollPane();
		scrllPaneDesignedGuards.setHorizontalScrollBarPolicy(
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrllPaneDesignedGuards.setVerticalScrollBarPolicy(
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrllPaneDesignedGuards.setBounds(472, 281, 166, 48);
		
		/*
		 ** j text area **
		 */
		textArea = new JTextArea();
		textArea.setWrapStyleWord(true);
		textArea.setBackground(Color.BLACK);
		textArea.setForeground(new Color(255, 153, 0));
		
		scrllPaneDesignedGuards.setViewportView(textArea);
			
		/*
		 ** j button **
		 */
		btnApplyChanges = new JButton("Aplicar cambios");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnApplyChanges);
		btnApplyChanges.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnApplyChanges.setBounds(472, 345, 144, 23);
	}
	
	public EditClientWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		add( lblCIInputInfo );
		add( lblDesignedGuards );
		add( lblGeneralInputInfo );
		add( lblHiredService );
		add( lblInputClientsCode );
		add( lblName );
		add( lblInputNewData );
		
		add( txtFClientsCode );
		add( txtFClientsName );
		
		add( scrllPaneDesignedGuards );
		add( textArea );
		
		add( btnApplyChanges );
	}
}
