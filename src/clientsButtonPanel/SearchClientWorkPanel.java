package clientsButtonPanel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import sharedClasses.DefaultComponentsConfiguration;

public class SearchClientWorkPanel extends JPanel {
	private static JRadioButton rdBtnSearch, rdBtnFilter;
	private static JLabel lblSearchWords;
	private static JTextField txtFSearchWordsInput;
	private static JButton btnSearchButton;
	
	static {
		rdBtnSearch = new JRadioButton("Buscar");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnSearch);
		rdBtnSearch.setSelected(true);
		rdBtnSearch.setFont(new Font("Tahoma", Font.BOLD, 14));
		rdBtnSearch.setBounds(193, 140, 97, 25);
		rdBtnSearch.addActionListener(new RdBtnSearchFilterClientListener());
			
		rdBtnFilter = new JRadioButton("Filtrar");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnFilter);
		rdBtnFilter.setFont(new Font("Tahoma", Font.BOLD, 14));
		rdBtnFilter.setBounds(292, 140, 97, 25);
		rdBtnFilter.addActionListener(new RdBtnSearchFilterClientListener());
			
		lblSearchWords = new JLabel("Ingrese palabras clave:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblSearchWords);
		lblSearchWords.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSearchWords.setBounds(193, 184, 154, 25);
			
		txtFSearchWordsInput = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFSearchWordsInput);
		txtFSearchWordsInput.setBounds(350, 187, 144, 20);
			
		btnSearchButton = new JButton("Buscar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnSearchButton);
		btnSearchButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSearchButton.setBounds(498, 185, 132, 23);
	}
	
	public SearchClientWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		add( rdBtnFilter );
		add( rdBtnSearch );
		add( lblSearchWords );
		add( txtFSearchWordsInput );
		add( btnSearchButton );
	}
}
