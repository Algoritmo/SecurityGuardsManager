package guardsPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;

import dataBaseMethods.InsertMethods;
import dataBaseMethods.ValidationMethods;
import sharedClasses.DefaultComponentsConfiguration;
import sharedClasses.MartialArtsList;
import sharedClasses.SharedMessages;
import sharedClasses.SharedMethods;
import sharedClasses.SharedValidationMethods;
import testing.SysoutSecondaryMartialArts;

public class AddGuardPhonesMartialArtsWorkPanel extends JPanel {
	private static ArrayList<String> phoneNumbers = new ArrayList<String>();
	private static String [] martialArts = MartialArtsList.getMartialArts();
	private static JLabel lblPhoneNumber, lblPhoneNumber1, lblPhoneNumber2, lblArteMarcialPrincipal,
								lblOtrasArtesMarciales;
	private static JTextField txtFPhoneNumber, txtFPhoneNumber1, txtFPhoneNumber2;
	private static JSpinner spnnrMainMartialArt, spnnrMartialArt2, spnnrMartialArt3, spnnrMartialArt4,
								spnnrMartialArt5;
	private static JButton btnClean, btnGoBack, btnSave;
	
	static {
		/*
		 ** j label **
		 */
		lblPhoneNumber = new JLabel("Tel\u00E9fono 1:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblPhoneNumber);
		lblPhoneNumber.setBounds(211, 144, 70, 14);
		
		lblPhoneNumber1 = new JLabel("Tel\u00E9fono 2:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblPhoneNumber1);
		lblPhoneNumber1.setBounds(337, 144, 70, 14);
		
		lblPhoneNumber2 = new JLabel("Tel\u00E9fono 3:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblPhoneNumber2);
		lblPhoneNumber2.setBounds(464, 144, 70, 14);
		
		lblArteMarcialPrincipal = new JLabel("Arte marcial principal:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblArteMarcialPrincipal);
		lblArteMarcialPrincipal.setBounds(211, 217, 132, 14);
		
		lblOtrasArtesMarciales = new JLabel("Artes marciales secundarias:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblOtrasArtesMarciales);
		lblOtrasArtesMarciales.setBounds(211, 244, 190, 14);
		
		
		
		/*
		 ** j textfield **
		 */
		txtFPhoneNumber = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFPhoneNumber);
		txtFPhoneNumber.setBounds(211, 169, 109, 20);
		
		txtFPhoneNumber1 = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFPhoneNumber1);
		txtFPhoneNumber1.setBounds(337, 169, 109, 20);
		
		txtFPhoneNumber2 = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFPhoneNumber2);
		txtFPhoneNumber2.setBounds(464, 169, 109, 20);
		
		
		
		/*
		 ** j spinner **
		 */
		spnnrMainMartialArt = new JSpinner();
		spnnrMainMartialArt.setModel(new SpinnerListModel(martialArts));
		spnnrMainMartialArt.setBounds(430, 217, 143, 18);
		
		spnnrMartialArt2 = new JSpinner();
		spnnrMartialArt2.setModel(new SpinnerListModel(martialArts));
		spnnrMartialArt2.setBounds(430, 244, 143, 18);
		
		spnnrMartialArt3 = new JSpinner();
		spnnrMartialArt3.setModel(new SpinnerListModel(martialArts));
		spnnrMartialArt3.setBounds(430, 269, 143, 18);
		
		spnnrMartialArt4 = new JSpinner();
		spnnrMartialArt4.setModel(new SpinnerListModel(martialArts));
		spnnrMartialArt4.setBounds(430, 298, 143, 18);
		
		spnnrMartialArt5 = new JSpinner();
		spnnrMartialArt5.setModel(new SpinnerListModel(martialArts));
		spnnrMartialArt5.setBounds(430, 325, 143, 18);
		
		
		
		/*
		 ** j button **
		 */
		btnSave = new JButton("Guardar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnSave);
		btnSave.setBounds(430, 378, 143, 23);
		
		btnGoBack = new JButton("Volver");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnGoBack);
		btnGoBack.setBounds(331, 378, 89, 23);
		
		btnClean = new JButton("Limpiar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnClean);
		btnClean.setBounds(243, 378, 89, 23);
	}
	
	public AddGuardPhonesMartialArtsWorkPanel() {
		setSize(650, 500);
		setOpaque(false);
		
		add( lblPhoneNumber );
		add( lblPhoneNumber1 );
		add( lblPhoneNumber2 );
		add( lblArteMarcialPrincipal );
		add( lblOtrasArtesMarciales );
		
		add( txtFPhoneNumber );
		add( txtFPhoneNumber1 );
		add( txtFPhoneNumber2 );
		
		add( spnnrMainMartialArt );
		add( spnnrMartialArt2 );
		add( spnnrMartialArt3 );
		add( spnnrMartialArt4 );
		add( spnnrMartialArt5 );
		
		btnSave.addActionListener(new ButtonsListener());
		btnClean.addActionListener(new ButtonsListener());
		btnGoBack.addActionListener(new ButtonsListener());
		
		add( btnClean );
		add( btnGoBack );
		add( btnSave );
	}

	class ButtonsListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			String fontOfEvent = e.getActionCommand();
			
			if ( fontOfEvent.equals(btnGoBack.getText()))
				SharedMethods.switchWorkPanelTo(new AddGuardWorkPanel ());
			
			else if ( fontOfEvent.equals( btnClean.getText() ) ) 
				cleanInputData();
			
			else if ( fontOfEvent.equals( btnSave.getText() ) ) {
				if ( allInputDataIsValid() ) {
					saveIntoDatabase();
					resetInputPanels();
					SharedMessages.changesWereAppliedSuccesfully();
				}
			}
		}
	}
	
	
	public static void cleanInputData () {
		txtFPhoneNumber.setText("");
		txtFPhoneNumber1.setText("");
		txtFPhoneNumber2.setText("");
		
		spnnrMainMartialArt.setValue(martialArts[0]);
		spnnrMartialArt2.setValue(martialArts[0]);
		spnnrMartialArt3.setValue(martialArts[0]);
		spnnrMartialArt4.setValue(martialArts[0]);
		spnnrMartialArt5.setValue(martialArts[0]);
	}
	
	private static boolean allInputDataIsValid () {
		boolean isValid = false;
		
		if ( !phoneNumberAndMartialArtWereInput() )
			JOptionPane.showMessageDialog(null, "Compruebe que haya ingresado al menos un"
					+ " n�mero de tel�fono y que haya seleccionado un arte marcial principal.", "Error",
					JOptionPane.ERROR_MESSAGE );
		
		else if ( !thereIsNoRepeatedElementIn( getInputMartialArts ()) )
			JOptionPane.showMessageDialog(null, "No pueden haber disciplinas repetidas.", "Error",
					JOptionPane.ERROR_MESSAGE );
		
		else if ( !thereIsNoRepeatedElementIn( getInputPhoneNumbers ()) )
			JOptionPane.showMessageDialog(null, "No pueden haber n�meros telef�nicos  repetidos.",
					"Error", JOptionPane.ERROR_MESSAGE );
		
		else if ( !phoneNumersAreValid())
			JOptionPane.showMessageDialog(null, "N�mero telef�nico incorrecto.\nS�lo pude ingresar "
					+ "n�meros (sin espacios).", "Error", JOptionPane.ERROR_MESSAGE);
		else
			isValid = true;
		
		return isValid;
	}
	
	private static void resetInputPanels () {
		AddGuardWorkPanel.cleanInputData();
		AddGuardPhonesMartialArtsWorkPanel.cleanInputData();
		SharedMethods.switchWorkPanelTo( new AddGuardWorkPanel () );
	}
	
	
	/*
	 ** saving into data base methods
	 */
	
	private static void saveIntoDatabase () {
		InsertMethods.insertGuard( AddGuardWorkPanel.guard );
		InsertMethods.insertMainMartialArt( AddGuardWorkPanel.guard.ci, spnnrMainMartialArt.getValue().toString() );
		savePhoneNumbers();
		
		if (thereAreSecondaryMartialArts( spnnrMartialArt2, spnnrMartialArt3, spnnrMartialArt4, spnnrMartialArt5))
			saveSecondaryMartialArts();
	}
	
	private static void savePhoneNumbers () {
		for ( int i = 0; i < phoneNumbers.size(); i++ ) {
			InsertMethods.insertInto("GuardsPhone", "ci, phoneNumber", 
					AddGuardWorkPanel.guard.ci + "', '" + phoneNumbers.get(i) );
		}
	}
	
	private static void saveSecondaryMartialArts () {
		ArrayList <String> secondaryMartialArts = new ArrayList<String>();
		secondaryMartialArts = getSecondaryMartialArts();
		
		for ( int i = 0; i < secondaryMartialArts.size(); i++)
			InsertMethods.insertSecondaryMartialArt(AddGuardWorkPanel.guard.ci, secondaryMartialArts.get(i));
	}
	
	/*
	 ** validation methods **
	 */
	
	// evaluates if at least the main martial art and a phone number were input
	private static boolean phoneNumberAndMartialArtWereInput () {
		boolean dataWasInput = true;
		
		if (	txtFPhoneNumber.getText().isEmpty()	&&	txtFPhoneNumber1.getText().isEmpty()
				&&	txtFPhoneNumber2.getText().isEmpty()	) {
			dataWasInput = false;
				
		} else if ( spnnrMainMartialArt.getValue().equals(martialArts[0])	) 
			dataWasInput = false;

		return dataWasInput;
	}
	
	private static boolean thereIsNoRepeatedElementIn (ArrayList <String> arrayList) {
		boolean thereIsNoRepeatedElement = true;
		
		if ( arrayList.size() > 1 ) {
			int i = 0, j = 0;
			
			while ( thereIsNoRepeatedElement	&&	i < arrayList.size() ) {
				while ( thereIsNoRepeatedElement	&&	 j < arrayList.size() ) {
					// if they're not the same element in the array, and they have same value
					if ( i != j 	&&	arrayList.get(i).equals(arrayList.get(j)) ) 
						thereIsNoRepeatedElement = false;
						
					j++;
				}
				i++;
			}
		}
		
		return thereIsNoRepeatedElement;
	}
	
	private static boolean phoneNumersAreValid() {
		boolean areValid = true;
		phoneNumbers.clear();
		phoneNumbers = getInputPhoneNumbers();
		
		if ( !checkPhoneNumberSize(phoneNumbers)	||	!checkIfPhonesHaveOnlyNumbers(phoneNumbers) )
			areValid = false;
		
		return areValid;
	}
	
	private static boolean checkPhoneNumberSize( ArrayList<String> phoneNumbers ) {
		boolean validSize = true;
		int size = 0;
		
		for (int i = 0; i < phoneNumbers.size(); i++) {
			size = phoneNumbers.get(i).length();
			if ( size != 9		&&	size != 8  ) {
				validSize = false;
				break;
			}
		}
		
		return validSize;
	}
	
	private static boolean checkIfPhonesHaveOnlyNumbers ( ArrayList<String> phoneNumbers) {
		boolean areValid = true;
		
		for (int i = 0; i < phoneNumbers.size(); i++)
			if ( !SharedValidationMethods.hasOnlyNumbers( phoneNumbers.get(i) ) ) {
				areValid = false;
				break;
			}
			
		return areValid;
	}
	
	private static boolean thereAreSecondaryMartialArts (JSpinner spinner1, JSpinner spinner2, JSpinner spinner3,
						JSpinner spinner4) {
		boolean thereAre = false;
		
		if ( !spinner1.getValue().equals(martialArts[0]) 	||	 !spinner2.getValue().equals(martialArts[0])		
				||	 !spinner3.getValue().equals(martialArts[0])	||	 !spinner4.getValue().equals(martialArts[0])  )
			thereAre = true;
		
		return thereAre;
	}
	
	
	/*
	 ** getters **
	 */
	
	private static ArrayList <String> getInputMartialArts () {
		ArrayList<String> martialArtsInput = new ArrayList <String>();
		
		// the main martial art was already validated
		martialArtsInput.add( spnnrMainMartialArt.getValue().toString() );

		String temporalSpinnerValue = spnnrMartialArt2.getValue().toString();
		if ( !temporalSpinnerValue.equals(martialArts[0]) )
			martialArtsInput.add( temporalSpinnerValue );
		
		temporalSpinnerValue = spnnrMartialArt3.getValue().toString();
		if ( !temporalSpinnerValue.equals(martialArts[0]) )
			martialArtsInput.add( temporalSpinnerValue );
		
		temporalSpinnerValue = spnnrMartialArt4.getValue().toString();
		if ( !temporalSpinnerValue.equals(martialArts[0]) )
			martialArtsInput.add( temporalSpinnerValue );
		
		temporalSpinnerValue = spnnrMartialArt5.getValue().toString();
		if ( !temporalSpinnerValue.equals(martialArts[0]) )
			martialArtsInput.add( temporalSpinnerValue );
		
		return martialArtsInput;
	}

	private static ArrayList <String> getInputPhoneNumbers () {
		ArrayList<String> phoneNumbersInput = new ArrayList <String>();
		String temporal;
		
		temporal = txtFPhoneNumber.getText();
		if ( !temporal.isEmpty() )
			phoneNumbersInput.add( temporal );
		
		temporal = txtFPhoneNumber1.getText();
		if ( !temporal.isEmpty() )
			phoneNumbersInput.add( temporal );
		
		temporal = txtFPhoneNumber2.getText();
		if ( !temporal.isEmpty() )
			phoneNumbersInput.add( temporal );
		
		return phoneNumbersInput;
	}

	private static ArrayList<String> getSecondaryMartialArts () {
		ArrayList<String> temporalArray = new ArrayList<String>();
		
		if ( !spnnrMartialArt2.getValue().equals(martialArts[0]) )
			temporalArray.add( spnnrMartialArt2.getValue().toString() );
		if ( !spnnrMartialArt3.getValue().equals(martialArts[0]) )
			temporalArray.add( spnnrMartialArt3.getValue().toString() );
		if ( !spnnrMartialArt4.getValue().equals(martialArts[0]) )
			temporalArray.add( spnnrMartialArt4.getValue().toString() );
		if ( !spnnrMartialArt5.getValue().equals(martialArts[0]) )
			temporalArray.add( spnnrMartialArt5.getValue().toString() );
			
		return temporalArray;
	}
}
