package guardsPanel;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import dataBaseMethods.Queries;

public class ListGuardsWorkPanel extends JPanel{
	private static JScrollPane scrollPane;
	private static JTextArea infoDisplayer = new JTextArea();
	
	static {
		scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(160, 140, 463, 343);
		
		infoDisplayer = new JTextArea();
		infoDisplayer.setForeground(new Color(255, 153, 0));
		infoDisplayer.setBackground(Color.BLACK);
		scrollPane.setViewportView(infoDisplayer);
		infoDisplayer.setEditable(false);
	}
	
	public ListGuardsWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		setGuardsData( infoDisplayer );
		add( scrollPane );
	}
	
	private static void setGuardsData ( JTextArea displayer ) {
		ArrayList<String> guardsData = Queries.getGuardsData();
		
		displayer.setText("Listado de guardias:\n");
		for ( int i = 0; i < guardsData.size(); i++ ) 
			displayer.append( guardsData.get( i ).toString() + "\n" );
	}
}
