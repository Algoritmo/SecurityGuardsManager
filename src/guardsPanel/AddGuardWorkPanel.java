package guardsPanel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import dataBaseMethods.InsertMethods;
import dataBaseMethods.ValidationMethods;
import sharedClasses.DefaultComponentsConfiguration;
import sharedClasses.Guard;
import sharedClasses.SharedMessages;
import sharedClasses.SharedMethods;
import sharedClasses.SharedValidationMethods;

public class AddGuardWorkPanel extends JPanel{
	public static Guard guard = new Guard();
	private static JButton btnClean, btnNext;
	private static JLabel lblInputNewData, lblFirstName, lblSecondName, lblInputInfo, lblFirstSurname, lblCi,
					lblWorkSchedule, lblSecondSurname, 	lblCarryingWeapon,	lblDrivingLicense, lblStreet,
					lblHouseNumber;
	private static JTextField txtFFirstName, txtFSecondName, txtFFirstSurname, txtFSecondSurname, txtFCI,
					txtFStreet, txtFHouseNumber;
	private static JRadioButton rdBtnFullTime, rdBtnPartTime, rdBtnHasCarryingWeapon,
					rdBtnHasDrivingLicense, rdBtnHasNoCarryingWeapon, rdBtnHasNoDrivingLicense;
	private static ButtonGroup btnGroupWorkSchedule, btnGroupCarryingWeapon, btnGroupDrivingLicense;
	
	// j components' initialization
	static {
		/*
		** labels **
		*/
		lblInputNewData = new JLabel("Ingrese los datos del nuevo guardia: ");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputNewData);
		lblInputNewData.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblInputNewData.setBounds(211, 136, 300, 20);
		
		lblInputInfo = new JLabel("(Los campos marcados con * son obligatorios.)");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputInfo);
		lblInputInfo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblInputInfo.setBounds(211, 156, 300, 16);

		lblFirstName = new JLabel("Primer nombre: *");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblFirstName);
		lblFirstName.setBounds(211, 198, 150, 14);
		
		lblSecondName = new JLabel("Segundo nombre:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblSecondName);
		lblSecondName.setBounds(211, 224, 150, 14);
		
		lblFirstSurname = new JLabel("Primer apellido: *");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblFirstSurname);
		lblFirstSurname.setBounds(211, 249, 150, 14);
		
		lblSecondSurname = new JLabel("Segundo apellido: *");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblSecondSurname);
		lblSecondSurname.setBounds(211, 274, 150, 14);
		
		lblCi = new JLabel("C.I.: *");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblCi);
		lblCi.setBounds(211, 299, 82, 14);
		
		lblStreet  = new JLabel("Calle: *");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblStreet);
		lblStreet.setBounds(211, 327, 150, 14);
		
		lblHouseNumber  = new JLabel("N�mero de puerta: *");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblHouseNumber);
		lblHouseNumber.setBounds(211, 352, 150, 14);
		
		lblWorkSchedule = new JLabel("Horario: *");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblWorkSchedule);
		lblWorkSchedule.setBounds(211, 379, 150, 14);
		
		lblCarryingWeapon = new JLabel("Porte de armas: *");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblCarryingWeapon);
		lblCarryingWeapon.setBounds(211, 404, 150, 14);
		
		lblDrivingLicense = new JLabel("Libreta de conducir: *");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblDrivingLicense);
		lblDrivingLicense.setBounds(211, 429, 150, 14);
		
		
		/*
		** j textfields **
		*/
		txtFFirstName = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFFirstName);
		txtFFirstName.setBounds(370, 197, 144, 20);
		
		txtFSecondName = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFSecondName);
		txtFSecondName.setBounds(370, 222, 144, 20);
		
		txtFFirstSurname = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFFirstSurname);
		txtFFirstSurname.setBounds(370, 247, 144, 20);
		
		txtFSecondSurname = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFSecondSurname);
		txtFSecondSurname.setBounds(370, 272, 144, 20);
		
		txtFCI = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFCI);
		txtFCI.setBounds(370, 298, 144, 20);
		
		txtFStreet = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFStreet);
		txtFStreet.setBounds(370, 324, 144, 20);
		
		txtFHouseNumber = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFHouseNumber);
		txtFHouseNumber.setBounds(370, 350, 65, 20);
		
		
		/*
		** radio buttons **
		*/
		rdBtnFullTime = new JRadioButton("Full time");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnFullTime);
		rdBtnFullTime.setBounds(363, 381, 82, 13);
		
		rdBtnPartTime = new JRadioButton("Part time");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnPartTime);
		rdBtnPartTime.setBounds(440, 381, 91, 13);
		
		rdBtnHasCarryingWeapon = new JRadioButton("S�");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasCarryingWeapon);
		rdBtnHasCarryingWeapon.setBounds(363, 406, 63, 13);
		
		rdBtnHasNoCarryingWeapon = new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasNoCarryingWeapon);
		rdBtnHasNoCarryingWeapon.setBounds(440, 406, 63, 13);
		
		rdBtnHasDrivingLicense = new  JRadioButton("S�");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasDrivingLicense);
		rdBtnHasDrivingLicense.setBounds(363, 431, 63, 13);
		
		rdBtnHasNoDrivingLicense = new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasNoDrivingLicense);
		rdBtnHasNoDrivingLicense.setBounds(440, 431, 57, 13);
		
		/*
		** button group **
		*/
		btnGroupWorkSchedule = new ButtonGroup();
		btnGroupWorkSchedule.add(rdBtnFullTime);
		btnGroupWorkSchedule.add(rdBtnPartTime);
		
		btnGroupCarryingWeapon = new ButtonGroup();
		btnGroupCarryingWeapon.add(rdBtnHasCarryingWeapon);
		btnGroupCarryingWeapon.add(rdBtnHasNoCarryingWeapon);
		
		btnGroupDrivingLicense = new ButtonGroup();
		btnGroupDrivingLicense.add(rdBtnHasDrivingLicense);
		btnGroupDrivingLicense.add(rdBtnHasNoDrivingLicense);
		
		/*
		** j buttons **
		*/
		btnClean = new JButton("Limpiar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnClean);
		btnClean.setBounds(300, 460, 110, 23);
		
		btnNext = new JButton("Siguiente");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnNext);
		btnNext.setBounds(410, 460, 110, 23);
	}
	
	public AddGuardWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		add( lblInputNewData);
		add( lblInputInfo);
		add( lblFirstName);
		add( lblSecondName );
		add( lblFirstSurname);
		add( lblSecondSurname);
		add( lblCi);
		add( lblStreet);
		add( lblHouseNumber);
		add( lblWorkSchedule);
		add( lblCarryingWeapon);
		add( lblDrivingLicense);
		
		add( txtFFirstName);
		add( txtFSecondName);
		add( txtFFirstSurname);
		add( txtFSecondSurname);
		add( txtFCI);
		add( txtFStreet);
		add( txtFHouseNumber);
		
		add( rdBtnFullTime);
		add( rdBtnHasCarryingWeapon);
		add( rdBtnHasDrivingLicense);
		add( rdBtnHasNoCarryingWeapon);
		add( rdBtnHasNoDrivingLicense);
		add( rdBtnPartTime);
		
		btnNext.addActionListener(new ButtonsListener());
		btnClean.addActionListener(new ButtonsListener());
		
		add( btnClean);
		add( btnNext);
	}
	
	public class ButtonsListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			String fontOfEvent = e.getActionCommand();
			
			if ( fontOfEvent.equals( btnNext.getText()) ) {
				if ( ValidationMethods.guardAlreadyExists( txtFCI.getText() ) ) {
					JOptionPane.showMessageDialog(null, "La c�dula de identidad ya fue ingresada "
							+ "a la base de datos.", "Error", JOptionPane.ERROR_MESSAGE );
				} else {
					if ( allInputDataIsValid() ) {
						saveDataOnRAM();
						SharedMethods.switchWorkPanelTo(new AddGuardPhonesMartialArtsWorkPanel());
					}
				}
			} else if ( fontOfEvent.equals(btnClean.getText()) )
				cleanInputData();
		}
	}
	
	
	public  static void cleanInputData () {
		txtFFirstName.setText("");
		txtFSecondName.setText("");
		txtFFirstSurname.setText("");
		txtFSecondSurname.setText("");
		txtFCI.setText("");
		txtFStreet.setText("");
		txtFHouseNumber.setText("");
		
		btnGroupCarryingWeapon.clearSelection();
		btnGroupDrivingLicense.clearSelection();
		btnGroupWorkSchedule.clearSelection();
	}
	
	private static void saveDataOnRAM () {
		guard.ci = txtFCI.getText();
		guard.firstName = txtFFirstName.getText();
		
		if ( !txtFSecondName.getText().isEmpty() )
			guard.secondName = txtFSecondName.getText();
		
		guard.firstSurname = txtFFirstSurname.getText();
		guard.secondSurname = txtFSecondSurname.getText();
		guard.street = txtFStreet.getText();
		guard.houseNumber = txtFHouseNumber.getText();
		
		guard.drivingLicense = ( rdBtnHasCarryingWeapon.isSelected() )	 ? 	"S"	 : 	"N";
		guard.weaponCarrying = ( rdBtnHasCarryingWeapon.isSelected() )	 ? 	"S"	 : 	"N";
		guard.workSchedule = ( rdBtnFullTime.isSelected() )	 ? 	"F"	 : 	"P";
		
		guard.category = getCategory( guard.drivingLicense, guard.weaponCarrying, guard.workSchedule );
	}
	
	private static String getCategory (String drivingLicense, String weaponCarrying, String workSchedule) {
		String category = "Bajo";
		
		if ( drivingLicense.equals("S")	&&		weaponCarrying.equals("S")
			&&		workSchedule.equals("F") ) 
				category = "VIP";
		
		// no carrying weapon license
		else if ( drivingLicense.equals("S")	&&		!weaponCarrying.equals("S")
			&&		workSchedule.equals("F") ) 
				category = "Alto A";
		
		// no driving license
		else if ( !drivingLicense.equals("S")	&&		weaponCarrying.equals("S")
			&&		workSchedule.equals("F") ) 
				category = "Alto B";
		
		// part time
		else if ( drivingLicense.equals("S")	&&		weaponCarrying.equals("S")
			&&		!workSchedule.equals("F") ) 
				category = "Alto C";
		
		//  no driving license, no carrying weapon license
		else if ( !drivingLicense.equals("S")	&&		!weaponCarrying.equals("S")
			&&		workSchedule.equals("F") ) 
				category = "Medio A";
		
		// no carrying weapon license, part time
		else if ( !drivingLicense.equals("S")	&&		!weaponCarrying.equals("S")
			&&		!workSchedule.equals("F") ) 
				category = "Medio B";
		
		// no driving license, part time
		else if ( !drivingLicense.equals("S")	&&		!weaponCarrying.equals("S")
			&&		workSchedule.equals("F") ) 
				category = "Medio C";
		
		return category;
	}
	
	/*
	 ** validation methods **
	 */
	private static boolean allInputDataIsValid () {
		boolean isValid = false;
		
		if ( thereIsAnEmptyTxtField() ) 
			SharedMessages.emptyFields();
		
		else if ( thereIsAnUnselectedOption() )
			SharedMessages.thereIsAnUnselectedOption();
		
		else if ( !SharedValidationMethods.ciIsValid( txtFCI.getText() ) )
			JOptionPane.showMessageDialog(null, "C�dula inv�lida. S�lo puede ingresar n�meros y debe"
					+ " tener 8 d�gitos.", "Error.", JOptionPane.ERROR_MESSAGE);
		
		else if ( !SharedValidationMethods.hasOnlyLetters( txtFFirstName.getText() ) )
			SharedMessages.inputOnlyLetters();
		
		else if ( !txtFSecondName.getText().isEmpty()		&&
				!SharedValidationMethods.hasOnlyLetters( txtFSecondName.getText() ) )
			SharedMessages.inputOnlyLetters();
		
		else if ( !SharedValidationMethods.hasOnlyLetters( txtFFirstSurname.getText()  ) )
			SharedMessages.inputOnlyLetters();
		
		else if ( !SharedValidationMethods.hasOnlyLetters( txtFSecondSurname.getText()  ) )
			SharedMessages.inputOnlyLetters();
		
		else if ( !houseNumberIsValid()  ) 
			JOptionPane.showMessageDialog(null, "El n�mero de calle es incorrecto.", "Error",
					JOptionPane.ERROR_MESSAGE);
		else
			isValid =  true;
		
		return isValid;
	}
	
	private static boolean thereIsAnEmptyTxtField () {
		if  ( txtFCI.getText().isEmpty()  	||	txtFFirstName.getText().isEmpty()	
				||	txtFFirstSurname.getText().isEmpty() 	||	txtFSecondSurname.getText().isEmpty() 
				||	txtFStreet.getText().isEmpty() 	||	txtFHouseNumber.getText().isEmpty() ) 
			return true;
		else
			return false;
	}
	
	private static boolean  thereIsAnUnselectedOption () {
		if ( (!rdBtnFullTime.isSelected())	&&	(!rdBtnPartTime.isSelected())	
			|| (!rdBtnHasCarryingWeapon.isSelected()) && (!rdBtnHasNoCarryingWeapon.isSelected())	
			|| (!rdBtnHasDrivingLicense.isSelected())	&&	(!rdBtnHasNoDrivingLicense.isSelected() ) )
				return true;
		else 
				return false;
	}
	
	private static boolean houseNumberIsValid () {
		String houseNumber = txtFHouseNumber.getText();
		
		if ( !houseNumber.isEmpty()		&&	 SharedValidationMethods.hasOnlyNumbers(houseNumber)
				&& 	(houseNumber.length() == 4	|| 	houseNumber.length() == 3 ) )
			return true;
		else
			return false;
	}
}
