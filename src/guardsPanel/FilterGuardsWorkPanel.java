package guardsPanel;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import sharedClasses.DefaultComponentsConfiguration;

public class FilterGuardsWorkPanel extends JPanel{
	private static JLabel lblWorkSchedule, lblCarryingWeapon, lblDrivingLicense, lblIsFree;
	private static JRadioButton rdBtnSearch, rdBtnFilter, rdBtnFullTime, rdBtnHasCarryingWeapon,
									rdBtnHasDrivingLicense, rdBtnParTime, rdBtnHasNoCarryingWeapon,
									rdBtnHasNoDrivingLicense, rdBtnIsFree, rdBtnIsNotFree;
	private static JCheckBox chkBxFilterWorkSchedule, chkBxFilterCarryingWeapon,
									chkBxFilterDrivingLicense, chkBxFilterIsFree;
	private static JButton btnFilter;
	
	// j components' initialization
	static {
		/*
		 ** j label **
		 */
		lblWorkSchedule = new JLabel("Horario: ");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblWorkSchedule);
		lblWorkSchedule.setBounds(191, 191, 136, 25);
		
		lblCarryingWeapon = new JLabel("Porte de armas:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblCarryingWeapon);
		lblCarryingWeapon.setBounds(191, 227, 136, 25);
		
		lblDrivingLicense = new JLabel("Libreta de conducir:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblDrivingLicense);
		lblDrivingLicense.setBounds(193, 263, 134, 25);
		
		lblIsFree = new JLabel("Disponible:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblIsFree);
		lblIsFree.setBounds(191, 299, 136, 25);
		
		/*
		 ** j radioButton **
		 */
		rdBtnSearch = new JRadioButton("Buscar");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnSearch);
		rdBtnSearch.setFont(new Font("Tahoma", Font.BOLD, 14));
		rdBtnSearch.setBounds(193, 140, 97, 25);
		rdBtnSearch.addActionListener(new RdBtnSearchFilterGuardsListener());
		
		rdBtnFilter = new JRadioButton("Filtrar");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnFilter);
		rdBtnFilter.setFont(new Font("Tahoma", Font.BOLD, 14));
		rdBtnFilter.setBounds(292, 140, 97, 25);
		rdBtnFilter.addActionListener(new RdBtnSearchFilterGuardsListener());
		
		rdBtnFullTime = new JRadioButton("Full time");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnFullTime);
		rdBtnFullTime.setBounds(351, 191, 82, 25);
		
		rdBtnHasCarryingWeapon = new JRadioButton("Si");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasCarryingWeapon);
		rdBtnHasCarryingWeapon.setEnabled(false);
		rdBtnHasCarryingWeapon.setBounds(351, 227, 63, 25);
		
		rdBtnHasDrivingLicense = new JRadioButton("Si");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasDrivingLicense);
		rdBtnHasDrivingLicense.setEnabled(false);
		rdBtnHasDrivingLicense.setBounds(351, 263, 63, 25);
		
		rdBtnParTime = new JRadioButton("Part time");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnParTime);
		rdBtnParTime.setBounds(430, 191, 91, 25);
		
		rdBtnHasNoCarryingWeapon = new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasNoCarryingWeapon);
		rdBtnHasNoCarryingWeapon.setEnabled(false);
		rdBtnHasNoCarryingWeapon.setBounds(430, 227, 63, 25);
		
		rdBtnHasNoDrivingLicense = new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasNoDrivingLicense);
		rdBtnHasNoDrivingLicense.setEnabled(false);
		rdBtnHasNoDrivingLicense.setBounds(430, 263, 57, 25);
		
		rdBtnIsFree = new JRadioButton("Si");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnIsFree);
		rdBtnIsFree.setEnabled(false);
		rdBtnIsFree.setBounds(351, 299, 63, 25);
		
		rdBtnIsNotFree =  new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnIsNotFree);
		rdBtnIsNotFree.setEnabled(false);
		rdBtnIsNotFree.setBounds(430, 299, 57, 25);
		
		/*
		 ** j checkBox **
		 */
		chkBxFilterWorkSchedule = new JCheckBox("");
		chkBxFilterWorkSchedule.setSelected(true);
		chkBxFilterWorkSchedule.setBounds(532, 193, 63, 23);
		chkBxFilterWorkSchedule.setBackground(Color.black);
		
		chkBxFilterCarryingWeapon = new JCheckBox("");
		chkBxFilterCarryingWeapon.setBounds(532, 229, 63, 23);
		chkBxFilterCarryingWeapon.setBackground(Color.black);
		
		chkBxFilterDrivingLicense = new JCheckBox("");
		chkBxFilterDrivingLicense.setBounds(532, 265, 63, 23);
		chkBxFilterDrivingLicense.setBackground(Color.black);
		
		chkBxFilterIsFree = new JCheckBox("");
		chkBxFilterIsFree.setBounds(532, 301, 63, 23);
		chkBxFilterIsFree.setBackground(Color.black);
		
		/*
		 ** j button **
		 */
		btnFilter = new JButton("Filtrar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnFilter);
		btnFilter.setBounds(357, 349, 136, 23);
	}

	public FilterGuardsWorkPanel() {
		setBounds(0, 0, 650, 500);
		setLayout(null);
		setOpaque(false);
	
		
		add( lblCarryingWeapon );
		add( lblDrivingLicense );
		add( lblIsFree );
		add( lblWorkSchedule );
		
		add( rdBtnFilter );
		add( rdBtnSearch );
		add( rdBtnFullTime );
		add( rdBtnParTime );
		add( rdBtnHasCarryingWeapon );
		add( rdBtnHasNoCarryingWeapon );
		add( rdBtnHasDrivingLicense );
		add( rdBtnHasNoDrivingLicense );
		add( rdBtnIsFree );
		add( rdBtnIsNotFree );
		
		add( chkBxFilterCarryingWeapon );
		add( chkBxFilterDrivingLicense );
		add( chkBxFilterIsFree );
		add( chkBxFilterWorkSchedule );
		
		add( btnFilter );
	}
}
