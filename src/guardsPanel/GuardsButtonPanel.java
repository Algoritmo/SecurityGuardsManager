package guardsPanel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JPanel;

import sharedClasses.DefaultComponentsConfiguration;
import sharedClasses.SharedComponents;

public class GuardsButtonPanel extends JPanel{
	private static JButton btnSearchGuard, btnEditGuardData, btnAddGuard, btnRemoveGuard,
								btnListGuards, btnMainMenu;
	
	// jComponents inicialization
	static {
		btnMainMenu =  SharedComponents.btnMainMenu();
		btnSearchGuard = new JButton("Buscar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnSearchGuard);
		btnSearchGuard.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSearchGuard.setBounds(20, 140, 132, 23);
		btnSearchGuard.addActionListener(new GuardsListener());
		
		btnEditGuardData = new JButton("Editar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnEditGuardData);
		btnEditGuardData.setBounds(20, 170, 132, 23);
		btnEditGuardData.addActionListener(new GuardsListener());
		
		btnAddGuard = new JButton("A�adir guardia");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnAddGuard);
		btnAddGuard.setBounds(20, 200, 132, 23);
		btnAddGuard.addActionListener(new GuardsListener());
		
		btnRemoveGuard = new JButton("Borrar guardia");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnRemoveGuard);
		btnRemoveGuard.setBounds(20, 230, 132, 23);
		btnRemoveGuard.addActionListener(new GuardsListener());
		
		btnListGuards = new JButton("Listar guardias");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnListGuards);
		btnListGuards.setBounds(20, 260, 132, 23);
		btnListGuards.addActionListener(new GuardsListener());
	}
	
	public GuardsButtonPanel() {
		setBounds(0, 0, 650, 500);
		setLayout(null);
		setOpaque(false);
		
		add( btnSearchGuard );
		add( btnAddGuard );
		add( btnEditGuardData );
		add( btnListGuards );
		add( btnRemoveGuard );
		
		add( btnMainMenu );
	}
}
