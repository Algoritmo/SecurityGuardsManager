package guardsPanel;

import javax.swing.JFrame;

import sharedClasses.SessionBackgroundPanel;

public class Testing extends JFrame {

	public Testing() {
		setUndecorated(true);
		setSize(650, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		add(new GuardsButtonPanel());
		add(new SessionBackgroundPanel());
	}

	public static void main(String[] args) {
		(new Testing()).setVisible(true);
	}

}
