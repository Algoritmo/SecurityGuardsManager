package guardsPanel;


import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import sharedClasses.DefaultComponentsConfiguration;

public class EditGuardWorkPanel extends JPanel {
	private static JLabel lblInputOldCI, lblInputNewData, lblGeneralInputInfo, lblNames, lblSurnames,
					lblCI, lblPhoneNumbers, lblMartialArts, lblWorkSchedules, lblWeaponCarrying,
					lblDrivingLicense, lblPhoneInputInfo, lblMartialArtsInputInfo  ;
	private static JTextField txtFInputOldCI, txtFNewName, txtFNewSurnames, txtFNewCI,
					txtFNewPhoneNumbers, txtFNewMartialArts;
	private static JRadioButton rdBtnFullTime, rdBtnPartTime, rdBtnHasCarryingWeapon,
					rdBtnHasNoCarryingWeapon, rdBtnHasDrivingLicense, rdBtnHasNoDrivingLicense;
	private static JButton btnApplyChanges, btnClean;
	
	static {
		/*
		 ** j labels **
		 */
		lblInputOldCI  = new JLabel("Ingrese la C.I. del guardia a modificar: ");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputOldCI);
		lblInputOldCI.setBounds(191, 141, 250, 14);
		lblInputOldCI.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		lblInputNewData = new JLabel("Ingrese datos nuevos:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputNewData);
		lblInputNewData.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblInputNewData.setBounds(191, 185, 162, 22);

		lblGeneralInputInfo = new JLabel("(Si no desea cambiar un dato, d\u00E9je la casilla en blanco)");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblGeneralInputInfo);
		lblGeneralInputInfo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblGeneralInputInfo.setBounds(191, 205, 278, 20);
		
		lblNames = new JLabel("Nombres:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblNames);
		lblNames.setBounds(191, 235, 82, 22);
		
		lblSurnames = new JLabel("Apellidos: ");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblSurnames);
		lblSurnames.setBounds(191, 252, 82, 22);
	
		lblCI = new JLabel("C.I. :");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblCI);
		lblCI.setBounds(191, 268, 82, 22);
		
		lblPhoneNumbers = new JLabel("Tel\u00E9fonos:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblPhoneNumbers);
		lblPhoneNumbers.setBounds(191, 285, 82, 22);
		
		lblMartialArts = new JLabel("Artes marciales :");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblMartialArts);
		lblMartialArts.setBounds(191, 320, 110, 21);

		lblWorkSchedules = new JLabel("Horario:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblWorkSchedules);
		lblWorkSchedules.setBounds(191, 370, 82, 22);
		
		lblWeaponCarrying = new JLabel("Porte de armas:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblWeaponCarrying);
		lblWeaponCarrying.setBounds(191, 387, 110, 22);

		lblDrivingLicense = new JLabel("Libreta de conducir: ");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblDrivingLicense);
		lblDrivingLicense.setBounds(191, 403, 127, 22);
		
		lblPhoneInputInfo  = new JLabel(
				"(Separe los tel\u00E9fonos con coma seguido de un espacio \", \")");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblPhoneInputInfo);
		lblPhoneInputInfo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblPhoneInputInfo.setBounds(191, 307, 312, 12);
		
		lblMartialArtsInputInfo = new JLabel("(Separe las disciplinas con coma seguido de un espacio \", \")");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblMartialArtsInputInfo);
		lblMartialArtsInputInfo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblMartialArtsInputInfo.setBounds(191, 342, 312, 13);

		
		/*
		 ** j txtFields **
		 */
		txtFInputOldCI = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFInputOldCI);
		txtFInputOldCI.setBounds(443, 143, 144, 19);
		
		txtFNewName = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFNewName);
		txtFNewName.setBounds(348, 236, 144, 19);
		
		txtFNewSurnames = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFNewSurnames);
		txtFNewSurnames.setBounds(348, 253, 144, 19);
		
		txtFNewCI = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFNewCI);
		txtFNewCI.setBounds(348, 270, 144, 19);
		
		txtFNewPhoneNumbers = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFNewPhoneNumbers);
		txtFNewPhoneNumbers.setBounds(348, 288, 289, 18);
		
		txtFNewMartialArts = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFNewMartialArts);
		txtFNewMartialArts.setBounds(348, 320, 286, 19);
		
		/*
		 ** j radioButtons **
		 */
		rdBtnFullTime = new JRadioButton("Full time");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnFullTime);
		rdBtnFullTime.setBounds(369, 376, 82, 13);
		
		rdBtnPartTime = new JRadioButton("Part time");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnPartTime);
		rdBtnPartTime.setBounds(448, 376, 91, 13);
		
		rdBtnHasCarryingWeapon = new JRadioButton("Si");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasCarryingWeapon);
		rdBtnHasCarryingWeapon.setBounds(369, 393, 63, 13);
		
		rdBtnHasNoCarryingWeapon = new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasNoCarryingWeapon);
		rdBtnHasNoCarryingWeapon.setBounds(448, 393, 63, 13);
		
		rdBtnHasDrivingLicense = new JRadioButton("Si");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasDrivingLicense);
		rdBtnHasDrivingLicense.setBounds(369, 409, 63, 13);
		
		rdBtnHasNoDrivingLicense = new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasNoDrivingLicense);
		rdBtnHasNoDrivingLicense.setBounds(448, 409, 57, 13);
		
		/*
		 ** j buttons **
		 */
		btnClean = new JButton("Limpiar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnClean);
		btnClean.setBounds(383, 455, 91, 23);
		
		btnApplyChanges = new JButton("Aplicar cambios");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnApplyChanges);
		btnApplyChanges.setBounds(484, 455, 138, 23);
		
	}

	public EditGuardWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		
		add( lblInputNewData );
		add( lblGeneralInputInfo );
		add( lblInputOldCI );
		add( lblCI );
		add( lblDrivingLicense );
		add( lblMartialArts );
		add( lblMartialArtsInputInfo );
		add( lblPhoneNumbers );
		add( lblPhoneInputInfo );
		add( lblNames );
		add( lblSurnames );
		add( lblWeaponCarrying );
		add( lblWorkSchedules );
		
		add( txtFInputOldCI );
		add( txtFNewCI );
		add( txtFNewName );
		add( txtFNewSurnames );
		add( txtFNewMartialArts );
		add( txtFNewPhoneNumbers );
		
		add( rdBtnFullTime );
		add( rdBtnPartTime );
		add( rdBtnHasCarryingWeapon );
		add( rdBtnHasNoCarryingWeapon );
		add( rdBtnHasDrivingLicense );
		add( rdBtnHasNoDrivingLicense );
		
		add( btnClean );
		add( btnApplyChanges );
	}
}
