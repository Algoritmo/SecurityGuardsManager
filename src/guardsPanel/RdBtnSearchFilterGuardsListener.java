package guardsPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import adminWindows.AdminWindow;
import mainWindow.MainWindow;
import sharedClasses.SharedMethods;

public class RdBtnSearchFilterGuardsListener implements ActionListener{

	public void actionPerformed(ActionEvent e) {
		String fontOfTheEvent = e.getActionCommand();
	
		MainWindow.mainFrame.remove(AdminWindow.workPanel);
	
		if (fontOfTheEvent.equals("Buscar") ) 
			AdminWindow.workPanel = new SearchGuardWorkPanel();
		else if (fontOfTheEvent.equals("Filtrar"))
			AdminWindow.workPanel = new FilterGuardsWorkPanel();
		
		MainWindow.mainFrame.add(AdminWindow.workPanel);
		SharedMethods.refreshPanels();
	}
}
