package guardsPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import sharedClasses.SharedMethods;

public class GuardsListener implements ActionListener{

	public void actionPerformed(ActionEvent e) {
		String fontOfTheEvent = e.getActionCommand();
		
		if (fontOfTheEvent.equals("Listar guardias")) 
			SharedMethods.switchWorkPanelTo(new ListGuardsWorkPanel());
		else if (fontOfTheEvent.equals("Editar"))
			SharedMethods.switchWorkPanelTo(new EditGuardWorkPanel());
		else if (fontOfTheEvent.equals("Borrar guardia"))
			SharedMethods.switchWorkPanelTo(new DeleteGuardWorkPanel());
		else if (fontOfTheEvent.equals("A�adir guardia"))
			SharedMethods.switchWorkPanelTo(new AddGuardWorkPanel());
		else if (fontOfTheEvent.equals("Buscar"))
			SharedMethods.switchWorkPanelTo(new SearchGuardWorkPanel());
	}
}
