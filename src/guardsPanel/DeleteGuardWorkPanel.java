package guardsPanel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.crypto.Cipher;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dataBaseMethods.DeleteGuardData;
import dataBaseMethods.Queries;
import dataBaseMethods.ValidationMethods;
import sharedClasses.DefaultComponentsConfiguration;
import sharedClasses.SharedMessages;
import sharedClasses.SharedValidationMethods;

public class DeleteGuardWorkPanel extends JPanel{
	private static JLabel lblInputCI;
	private static JTextField txtFCI;
	private static JButton btnRemove;
	
	//j components' initialization
	static {
		lblInputCI = new JLabel("Ingrese la C.I. del guardia a borrar: ");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblInputCI);
		lblInputCI.setBounds(191, 144, 250, 14);
		lblInputCI.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		txtFCI = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFCI);
		txtFCI.setBounds(443, 141, 132, 19);
		
		btnRemove = new JButton("Borrar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnRemove);
		btnRemove.setBounds(443, 165, 132, 23);
		
	}

	public DeleteGuardWorkPanel () {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		btnRemove.addActionListener( new DeleteAction() );
		
		add( lblInputCI );
		add( txtFCI );
		add( btnRemove );
	}
	
	private class DeleteAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String ci = txtFCI.getText();
			
			if ( !SharedValidationMethods.ciIsValid( ci ) )
				SharedMessages.invalidCi();
			else if ( !SharedValidationMethods.hasOnlyNumbers( ci ) )
				SharedMessages.inputOnlyNumbers();
			else if ( !ValidationMethods.guardAlreadyExists( ci ) )
				SharedMessages.guardDoesntExist();
			else 
				deleteGuardConfirmationMessage( ci );
		}
	}

	private static void deleteGuardConfirmationMessage ( String ci ) {
		Object [] options = { "Si", "No" };
		String [] data = Queries.getBasicGuardData( ci );
		int answer = JOptionPane.showOptionDialog(new JFrame(), "�Desea borrar todos los datos del siguiente"
				+ " guardia? (Los cambios no podr�n ser deshechos)\n"
				+ "Nombre completo:" + data[0] + " " + data[1] + " " + data[2] + " " + data[3] + " " 
				+ "\nC.I.: " + ci
				+ "\nCategoria: " + data[4]
				+ "\nDirecci�n: " + data[5] + " " + data[6] , "Confirmaci�n", JOptionPane.YES_NO_OPTION,
				JOptionPane.INFORMATION_MESSAGE, null, options, options[ 1 ]);
		
		if ( answer == 0 )
			DeleteGuardData.deleteAll( ci );
		
		SharedMessages.changesWereAppliedSuccesfully();
	}
}
