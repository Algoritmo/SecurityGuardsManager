// agregar filtro por categor�a

package guardsPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;

import sharedClasses.DefaultComponentsConfiguration;
import sharedClasses.SharedMessages;
import sharedClasses.SharedMethods;

public class SearchGuardWorkPanel extends JPanel{
	private static JRadioButton rdBtnFullTime, rdBtnHasCarryingWeapon,
							rdBtnHasDrivingLicense, rdBtnParTime, rdBtnHasNoCarryingWeapon,
							rdBtnHasNoDrivingLicense, rdBtnIsFree, rdBtnIsNotFree;
	private static JLabel lblSearchWord, lblWorkSchedule, lblCarryingWeapon, lblDrivingLicense,
							lblIsFree, lblCategory;
	private static JCheckBox chkBxFilterWorkSchedule, chkBxFilterCarryingWeapon, chkBxCategory,
							chkBxFilterDrivingLicense, chkBxFilterIsFree, chkBxFilterSearchWord;
	private static ButtonGroup btnGroupWorkSchedule, btnGroupDrivingLicense,
							btnGroupCarryingWeapon, btnGroupIsFree;
	private static JTextField txtFSearchWordsInput;
	private static JButton btnSearch;
	private static JSpinner spnnrCategoryOptions;
	private final static String [] categoryOptions = {"VIP", "Alto", "Medio", "Bajo"};
	
	static {
		/*
		 ** j radioButton **
		 */
		rdBtnFullTime = new JRadioButton("Full time");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnFullTime);
		rdBtnFullTime.setBounds(351, 191, 82, 25);
		
		rdBtnParTime = new JRadioButton("Part time");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnParTime);
		rdBtnParTime.setBounds(430, 191, 91, 25);
		
		rdBtnHasDrivingLicense = new JRadioButton("Si");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasDrivingLicense);
		rdBtnHasDrivingLicense.setEnabled(false);
		rdBtnHasDrivingLicense.setBounds(351, 263, 63, 25);
		
		rdBtnHasNoDrivingLicense = new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasNoDrivingLicense);
		rdBtnHasNoDrivingLicense.setEnabled(false);
		rdBtnHasNoDrivingLicense.setBounds(430, 263, 57, 25);
		
		rdBtnHasCarryingWeapon = new JRadioButton("Si");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasCarryingWeapon);
		rdBtnHasCarryingWeapon.setEnabled(false);
		rdBtnHasCarryingWeapon.setBounds(351, 227, 63, 25);
		
		rdBtnHasNoCarryingWeapon = new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnHasNoCarryingWeapon);
		rdBtnHasNoCarryingWeapon.setEnabled(false);
		rdBtnHasNoCarryingWeapon.setBounds(430, 227, 63, 25);
		
		rdBtnIsFree = new JRadioButton("Si");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnIsFree);
		rdBtnIsFree.setEnabled(false);
		rdBtnIsFree.setBounds(351, 299, 63, 25);
		
		rdBtnIsNotFree =  new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnIsNotFree);
		rdBtnIsNotFree.setEnabled(false);
		rdBtnIsNotFree.setBounds(430, 299, 57, 25);
		
		
		/*
		 ** j spinner **
		 */
		spnnrCategoryOptions = new JSpinner();
		spnnrCategoryOptions.setModel(new SpinnerListModel(categoryOptions));
		spnnrCategoryOptions.setEnabled(false);
		spnnrCategoryOptions.setBounds(430, 338, 65, 18);
		
		
		/*
		 ** button group **
		 */
		rdBtnIsNotFree =  new JRadioButton("No");
		DefaultComponentsConfiguration.setDefaultRdBtnConfiguration(rdBtnIsNotFree);
		rdBtnIsNotFree.setEnabled(false);
		rdBtnIsNotFree.setBounds(430, 299, 57, 25);
		
		
		/*
		 ** button group **
		 */
		btnGroupWorkSchedule =  new ButtonGroup();
		btnGroupWorkSchedule.add(rdBtnFullTime);
		btnGroupWorkSchedule.add(rdBtnParTime);
		
		btnGroupDrivingLicense = new ButtonGroup();
		btnGroupDrivingLicense.add(rdBtnHasDrivingLicense);
		btnGroupDrivingLicense.add(rdBtnHasNoDrivingLicense);

		btnGroupCarryingWeapon = new ButtonGroup();
		btnGroupCarryingWeapon.add(rdBtnHasCarryingWeapon);
		btnGroupCarryingWeapon.add(rdBtnHasNoCarryingWeapon);
		
		btnGroupIsFree  = new ButtonGroup();
		btnGroupIsFree.add(rdBtnIsFree);
		btnGroupIsFree.add(rdBtnIsNotFree);
		
		
		/*
		 ** j label **
		 */
		lblSearchWord = new JLabel("Ingrese palabras clave:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblSearchWord);
		lblSearchWord.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSearchWord.setBounds(191, 140, 154, 25);
		
		lblWorkSchedule = new JLabel("Horario: ");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblWorkSchedule);
		lblWorkSchedule.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblWorkSchedule.setBounds(191, 188, 136, 25);
		
		lblCarryingWeapon = new JLabel("Porte de armas:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblCarryingWeapon);
		lblCarryingWeapon.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCarryingWeapon.setBounds(191, 226, 136, 25);
		
		lblDrivingLicense = new JLabel("Libreta de conducir:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblDrivingLicense);
		lblDrivingLicense.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDrivingLicense.setBounds(191, 262, 134, 25);
		
		lblIsFree = new JLabel("Disponible:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblIsFree);
		lblIsFree.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblIsFree.setBounds(191, 298, 136, 25);
		
		lblCategory = new JLabel("Categor�a:");
		DefaultComponentsConfiguration.setDefaultLabelConfiguration(lblCategory);
		lblCategory.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCategory.setBounds(191, 334, 136, 25);
		
		
		/*
		 ** j textField **
		 */
		txtFSearchWordsInput = new JTextField();
		DefaultComponentsConfiguration.setDefaultTxtFConfiguration(txtFSearchWordsInput);
		txtFSearchWordsInput.setBounds(350, 144, 144, 18);
		
		
		/*
		 ** j checkBox **
		 */
		chkBxFilterSearchWord = new JCheckBox("");
		chkBxFilterSearchWord.setSelected(true);
		chkBxFilterSearchWord.setBounds(532, 144, 63, 23);
		chkBxFilterSearchWord.setBackground(Color.black);
		
		chkBxFilterWorkSchedule = new JCheckBox("");
		chkBxFilterWorkSchedule.setSelected(true);
		chkBxFilterWorkSchedule.setBounds(532, 193, 63, 23);
		chkBxFilterWorkSchedule.setBackground(Color.black);
		
		chkBxFilterCarryingWeapon = new JCheckBox("");
		chkBxFilterCarryingWeapon.setBounds(532, 229, 63, 23);
		chkBxFilterCarryingWeapon.setBackground(Color.black);
		
		chkBxFilterDrivingLicense = new JCheckBox("");
		chkBxFilterDrivingLicense.setBounds(532, 265, 63, 23);
		chkBxFilterDrivingLicense.setBackground(Color.black);
		
		chkBxFilterIsFree = new JCheckBox("");
		chkBxFilterIsFree.setBounds(532, 301, 63, 23);
		chkBxFilterIsFree.setBackground(Color.black);
		
		chkBxCategory = new JCheckBox("");
		chkBxCategory.setBounds(532, 334, 63, 23);
		chkBxCategory.setBackground(Color.black);
		
		
		/*
		 ** j button **
		 */
		btnSearch = new JButton("Buscar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnSearch);
		btnSearch.setBounds(357, 380, 136, 23);
	}
	
	public SearchGuardWorkPanel() {
		setBounds(0, 0, 650, 500);
		setOpaque(false);
		setLayout(null);
		
		add( lblSearchWord );
		add( txtFSearchWordsInput );
		
		add(spnnrCategoryOptions);
		
		add( lblWorkSchedule );
		add( lblCarryingWeapon );
		add( lblDrivingLicense );
		add( lblIsFree );
		add( lblCategory );
		
		chkBxFilterWorkSchedule.addMouseListener(new CheckBoxListener());
		chkBxFilterCarryingWeapon.addMouseListener(new CheckBoxListener());
		chkBxFilterDrivingLicense.addMouseListener(new CheckBoxListener());
		chkBxFilterIsFree.addMouseListener(new CheckBoxListener());
		chkBxFilterSearchWord.addMouseListener(new CheckBoxListener());
		chkBxCategory.addMouseListener(new CheckBoxListener());
		
		add( chkBxFilterWorkSchedule );
		add( chkBxFilterCarryingWeapon );
		add( chkBxFilterDrivingLicense );
		add( chkBxFilterIsFree );
		add( chkBxFilterSearchWord );
		add( chkBxCategory );
		
		add( rdBtnFullTime );
		add( rdBtnParTime );
		add( rdBtnHasCarryingWeapon );
		add( rdBtnHasNoCarryingWeapon );
		add( rdBtnHasDrivingLicense );
		add( rdBtnHasNoDrivingLicense );
		add( rdBtnIsFree );
		add( rdBtnIsNotFree );
		
		btnSearch.addActionListener(new ButtonListener());
		add( btnSearch );
	}
	
	public class CheckBoxListener extends MouseAdapter {
		
		public void mouseClicked (MouseEvent e) {
			JCheckBox temporal = (JCheckBox) e.getSource();
			int yPosition = temporal.getY();
			
			if (yPosition == chkBxFilterSearchWord.getY())
				txtFSearchWordsInput.setEnabled(!txtFSearchWordsInput.isEnabled());
			
			else if (yPosition == chkBxFilterWorkSchedule.getY()) {
				rdBtnFullTime.setEnabled(!rdBtnFullTime.isEnabled());
				rdBtnParTime.setEnabled(!rdBtnParTime.isEnabled());
				
			} else if (yPosition == chkBxFilterCarryingWeapon.getY()) {
				rdBtnHasCarryingWeapon.setEnabled(!rdBtnHasCarryingWeapon.isEnabled());
				rdBtnHasNoCarryingWeapon.setEnabled(!rdBtnHasNoCarryingWeapon.isEnabled());
				
			} else if (yPosition == chkBxFilterDrivingLicense.getY())  {
				rdBtnHasDrivingLicense.setEnabled(!rdBtnHasDrivingLicense.isEnabled());
				rdBtnHasNoDrivingLicense.setEnabled(!rdBtnHasNoDrivingLicense.isEnabled());
				
			} else if (yPosition == chkBxFilterIsFree.getY())  {
				rdBtnIsFree.setEnabled(!rdBtnIsFree.isEnabled());
				rdBtnIsNotFree.setEnabled(!rdBtnIsNotFree.isEnabled());
				
			}  else if (yPosition == chkBxCategory.getY())  {
				spnnrCategoryOptions.setEnabled(!spnnrCategoryOptions.isEnabled());
			}
		}
	}
	
	public class ButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			
			if (!chkBxFilterWorkSchedule.isSelected()	&&	!chkBxFilterCarryingWeapon.isSelected()
				&&	!chkBxFilterDrivingLicense.isSelected()	&&	 !chkBxFilterIsFree.isSelected()
				&&	!chkBxFilterSearchWord.isSelected()	&&	!chkBxCategory.isSelected() )
					SharedMessages.selectAnOption();
			else {
				
				if (chkBxFilterSearchWord.isSelected()		&&	txtFSearchWordsInput.getText().isEmpty())
					JOptionPane.showMessageDialog(null, "No ha ingresado una palabra a buscar.", "Error",
							JOptionPane.ERROR_MESSAGE);
					
//				if ( chkBxCategory.isSelected()	 &&	!SharedMethods.arrayContainsString(categoryOptions,
//								(String) spnnrCategoryOptions.getValue() ) ) 
//					JOptionPane.showMessageDialog(null, "La categor�a es inv�lida.", "Error",
//							JOptionPane.ERROR_MESSAGE);
//					
			}
		}
	}
}
