package sharedClasses;

public class Guard {
	public String ci, firstName, secondName, firstSurname, secondSurname, street, houseNumber,
		drivingLicense, weaponCarrying, workSchedule, category;
	
	public Guard() {
	}
	
	public Guard(String ci, String firstName, String secondName, String firstSurname, String street,
		String houseNumber, String drivingLicense, String weaponCarrying, String workSchedule,
		String category) {
			
			this.ci = ci;
			this.firstName = firstName;
			this.secondName = secondName;
			this.firstSurname = firstSurname;
			this.street = street;
			this.houseNumber = houseNumber;
			this.drivingLicense = drivingLicense;
			this.weaponCarrying = weaponCarrying;
			this.workSchedule = workSchedule;
			this.category = category;
	}
	
	public Guard( String ci, String firstName, String secondName, String firstSurname, String secondSurname,
		String street, String houseNumber, String drivingLicense, String weaponCarrying, String workSchedule,
		String category ) {
		
			this.ci = ci;
			this.firstName = firstName;
			this.secondName = secondName;
			this.firstSurname = firstSurname;
			this.secondSurname = secondSurname;
			this.street = street;
			this.houseNumber = houseNumber;
			this.drivingLicense = drivingLicense;
			this.weaponCarrying = weaponCarrying;
			this.workSchedule = workSchedule;
			this.category = category;
	}
	
	public void getGuardData () {
		System.out.println( ci + " - " + firstName + " - " + secondName+  " - " 
				+ firstSurname + " - " + secondSurname + " - " + street + " - " 
				+ houseNumber	+ " - " + drivingLicense + " - " + weaponCarrying 
				+ " - " + workSchedule + " - " + category);
	}
}
