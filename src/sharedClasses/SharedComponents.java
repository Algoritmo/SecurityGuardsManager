package sharedClasses;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;


public class SharedComponents {
	
	public static JLabel lblSessionBackground () {
		ImageIcon icon = new ImageIcon(SharedComponents.class.getResource("/fondoVentanas.png"));
		
		JLabel label = new JLabel("");
		label.setBounds(0, 0, 650, 500);
		label.setIcon(icon);
		
		return label;
	}
	
	public static JButton btnMainMenu () {
		JButton mainMenu = new JButton("Menu principal");
			
		mainMenu.setFont(new Font("Tahoma", Font.BOLD, 13));
		mainMenu.setBackground(new Color(0, 0, 0));
		mainMenu.setForeground(new Color(255, 51, 0));
		mainMenu.setBounds(20, 400, 132, 23);
		mainMenu.addActionListener(new SharedListeners());
		
		return mainMenu;
	}
	
	public static JButton btnLogOut () {
		JButton logOut = new JButton("Cerrar sesi�n");
		
		logOut.setFont(new Font("Tahoma", Font.BOLD, 13));
		logOut.setBackground(new Color(0, 0, 0));
		logOut.setForeground(new Color(255, 51, 0));
		logOut.setBounds(20, 430, 132, 23);
		logOut.addActionListener(new SharedListeners());
		
		return logOut;
	}
	
	public static JButton btnExit () {
		JButton btnSalir = new JButton("Salir");
		
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSalir.setBackground(new Color(0, 0, 0));
		btnSalir.setForeground(new Color(255, 51, 0));
		btnSalir.setBounds(20, 460, 132, 23);
		btnSalir.addActionListener(new SharedListeners());
		
		return btnSalir;
	}
}
