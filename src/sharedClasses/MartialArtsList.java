package sharedClasses;

import java.util.ArrayList;

import dataBaseMethods.Queries;

public class MartialArtsList {
	
	public static String[] getMartialArts () {
		ArrayList<String> martialArtsTmp = Queries.getMartialArts();
		int size = martialArtsTmp.size();
		String [] martialArts = new String [size];
		
		for ( int i = 0; i < size; i++ )
			martialArts[i] = martialArtsTmp.get(i);
		
		return martialArts;
	}
}
