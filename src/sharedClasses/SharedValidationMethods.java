package sharedClasses;


public class SharedValidationMethods {
	
	public static boolean ciIsValid( String ci ) {
		boolean isValid = false;
		
		if ( ci.length() == 8	 &&	hasOnlyNumbers(ci) )
			isValid = true;
		
		return isValid;
	}
	
	public static boolean hasOnlyNumbers ( String string ) {
		boolean hasOnlyNumbers = true;
		
		for ( int i = 0; i < string.length(); i++ ) 
			// this IF evaluates the char's ascii code (48 is 0 (zero), 57 is 9)
			if ( string.codePointAt(i)  < 48	||	string.codePointAt(i) > 57 ) {
				hasOnlyNumbers = false;
				break;
			}
		
		return hasOnlyNumbers;
	}
	
	public static boolean hasOnlyLetters ( String string ) {
		boolean hasOnlyLetters = true;
		int currentCharAsciiCode = 0;
		
		for (int i = 0; i < string.length(); i++)  {
			currentCharAsciiCode = string.codePointAt(i);
			// the IF statement evaluates the char's ascii code
			if (	(currentCharAsciiCode != 45	&&	currentCharAsciiCode < 65 )
					||	(currentCharAsciiCode > 90		&& 	currentCharAsciiCode < 97)
					||	(currentCharAsciiCode > 122		&&	currentCharAsciiCode < 128)
					||	(currentCharAsciiCode > 154		&&	currentCharAsciiCode < 160)
					||	(currentCharAsciiCode > 165		&&	currentCharAsciiCode < 181)
					||	(currentCharAsciiCode > 183		&&	currentCharAsciiCode < 198)
					||	(currentCharAsciiCode > 199		&&	currentCharAsciiCode < 210)
					||	(currentCharAsciiCode > 212		&&	currentCharAsciiCode < 224)
					||	currentCharAsciiCode > 237	) {
							hasOnlyLetters = false;
							break;
			}
		}
		return hasOnlyLetters;
	}
}
