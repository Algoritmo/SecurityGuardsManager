package sharedClasses;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class DefaultComponentsConfiguration {
	
	public static void setDefaultButtonConfiguration (JButton defaultButton) {
		defaultButton.setBackground(Color.black);
		defaultButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		defaultButton.setForeground(new Color(255, 51, 0));
	}
	
	public static void setDefaultLabelConfiguration (JLabel jLabel) {
		jLabel.setBackground(Color.black);
		jLabel.setForeground(new Color(255, 160, 0));
		jLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		jLabel.setOpaque(true);
		jLabel.setHorizontalAlignment(SwingConstants.LEFT);
	}
	
	public static void setDefaultTxtFConfiguration (JTextField textField) {
		textField.setForeground(Color.YELLOW);
		textField.setBackground(Color.BLACK);
	}
	
	public static void setDefaultRdBtnConfiguration (JRadioButton radioButton) {
		radioButton.setForeground(Color.YELLOW);
		radioButton.setFont(new Font("Tahoma", Font.PLAIN, 13));
		radioButton.setBackground(Color.BLACK);
	}
}
