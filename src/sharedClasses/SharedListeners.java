package sharedClasses;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import adminWindows.AdminWindow;
import mainMenu.MainMenuButtonPanel;
import mainWindow.MainWindow;

public class SharedListeners implements ActionListener{

	public void actionPerformed(ActionEvent e) {

		String fontOfTheEvent = e.getActionCommand();
		
		
		if (fontOfTheEvent.equals("Salir")) 
			System.exit(0);
		else if (fontOfTheEvent.equals("Cerrar sesi�n"))
			SharedMethods.logOut();
		else if (fontOfTheEvent.equals("Menu principal")) {
			MainWindow.mainFrame.remove(AdminWindow.buttonPanel);
			AdminWindow.buttonPanel = new MainMenuButtonPanel();
			MainWindow.mainFrame.add( AdminWindow.buttonPanel );
			
			 SharedMethods.refreshPanels();
		}
	}
}
