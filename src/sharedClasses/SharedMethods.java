package sharedClasses;

import java.awt.Component;

import javax.swing.JPanel;

import adminWindows.AdminWindow;
import login.LoginWindow;
import mainWindow.MainWindow;

public class SharedMethods {

	public static void logOut () {
		MainWindow.mainFrame.dispose();
		MainWindow.mainFrame = new LoginWindow();;
		MainWindow.mainFrame.setVisible(true);
	}
	
	public static void switchWorkPanelTo (JPanel newWorkPanel) {
		MainWindow.mainFrame.remove(AdminWindow.workPanel);
		AdminWindow.workPanel = newWorkPanel;
		MainWindow.mainFrame.add(AdminWindow.workPanel);
		SharedMethods.refreshPanels();
	}
	
	public static void refreshPanels() {
		MainWindow.mainFrame.update(MainWindow.mainFrame.getGraphics());
		SharedMethods.redrawComponents(AdminWindow.workPanel);
		
		MainWindow.mainFrame.update(MainWindow.mainFrame.getGraphics());
		SharedMethods.redrawComponents(AdminWindow.buttonPanel);
	}
	
	
	public static void redrawComponents (JPanel panel) {
		Component[] components = panel.getComponents();
            Component component = null;

            for (int i = 0; i < components.length; i++) {
            	 component = components[i];
            	 component.validate();
            	 component.repaint();
            }
	}
	
	public static boolean arrayContainsString (String [] staticArray, String string) {
		boolean containsIt = false;
		
		for (int i = 0; i < staticArray.length; i++) 
			if ( staticArray[i].equals(string))
				containsIt = true;
			
			return containsIt;
	}
}
