package sharedClasses;

import javax.swing.JPanel;

public class SessionBackgroundPanel extends JPanel{

	public SessionBackgroundPanel() {
		setLayout(null);
		setBounds(0, 0, 650, 500);
		
		add( SharedComponents.btnLogOut() );
		add( SharedComponents.btnExit() );
		add( SharedComponents.lblSessionBackground() );
	}
}
