package sharedClasses;

import javax.swing.JOptionPane;

public class SharedMessages {
	
	public static void selectAnOption () {
		JOptionPane.showMessageDialog(null, "Debe seleccionar al menos una opci�n.", "Error",
				JOptionPane.ERROR_MESSAGE);
	}
	
	public static void emptyFields () {
		JOptionPane.showMessageDialog(null, "No pueden haber campos vac�os.", "Error",
				JOptionPane.ERROR_MESSAGE);
	}
	
	public static void thereIsAnUnselectedOption () {
		JOptionPane.showMessageDialog(null, "No pueden haber opciones sin seleccionar.", "Error",
				JOptionPane.ERROR_MESSAGE);
	}
	
	public static void  inputOnlyLetters() {
		JOptionPane.showMessageDialog(null, "Error. Ha ingresado caracteres especiales o n�meros en un "
				+ "campo que s�lo admite letras.", "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void changesWereAppliedSuccesfully () {
		JOptionPane.showMessageDialog(null, "Los cambios fueron realizados exitosamente.", "Mensaje",
				JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void inputOnlyNumbers () {
		JOptionPane.showMessageDialog( null,  "La c�dula s�lo puede contener n�meros.");
	}
	
	public static void guardDoesntExist () {
		JOptionPane.showMessageDialog( null, "El guardia no se encuentra registrado en la base de datos");
	}
	
	public static void invalidCi () {
		
	}
}
