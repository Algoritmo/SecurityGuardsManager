package login;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class LoginWindow extends JFrame {
	
	public static JPanel currentPanel = new LoginPanel();
	
	public LoginWindow () {
		String path = "resources/icon.png";  
		ImageIcon icon = new ImageIcon(path); 
		setIconImage(icon.getImage());
		
		setSize(350, 500);
		setUndecorated(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		add(currentPanel);
	}
}
