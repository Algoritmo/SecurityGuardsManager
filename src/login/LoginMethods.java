package login;

import javax.swing.JFrame;

import adminWindows.AdminWindow;
import mainWindow.MainWindow;

public class LoginMethods {
	
	public static boolean checkUserAndPassword (String userName, String password) {
		boolean checking = false;
		
		if (userName.equals("Administrador")) 
				if (password.equals("password666")) 
					checking = true;
		else if (userName.equals("Usuario"))
				if (password.equals("password666"))
					checking = true;
		
		return checking;
	}
	
	public static void openSessionWindow(String userName) {
		JFrame sessionWindow = new JFrame();
		
		if (userName.equals("Administrador")) 
			sessionWindow = new AdminWindow();
//		else if (userName.equals("Usuario"))
//			sessionWindow = new UserWindow();
		
		MainWindow.mainFrame.dispose();
		MainWindow.mainFrame = sessionWindow;
		MainWindow.mainFrame.setVisible(true);

		LoginPanel.cleanTxtPassword();
		LoginPanel.cleanTxtUsuario();
	}
}
