package login;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import sharedClasses.SharedListeners;

class LoginPanel extends JPanel {
	private static JLabel lblBackground, lblUsername, lblPassword;
	private static JButton btnLogin, btnExit;
	private static JPasswordField txtFPassword;
	private static JTextField txtFUsuario;
	
	// j components' initialization
	static {
		txtFUsuario = new JTextField();
		txtFUsuario.setForeground(Color.ORANGE);
		txtFUsuario.setBackground(Color.BLACK);
		txtFUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		txtFUsuario.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtFUsuario.setBounds(169, 316, 146, 20);
		txtFUsuario.setText("Administrador");
		
		txtFPassword = new JPasswordField();
		txtFPassword.setForeground(Color.ORANGE);
		txtFPassword.setBackground(Color.BLACK);
		txtFPassword.setHorizontalAlignment(SwingConstants.CENTER);
		txtFPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtFPassword.setBounds(169, 359, 146, 20);
		txtFPassword.setText("password666");
		
		
		lblUsername = new JLabel("Usuario:");
		lblUsername.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUsername.setBounds(27, 316, 132, 20);
		lblUsername.setForeground(new Color(255, 51, 0));
		
		lblPassword = new JLabel("Clave:");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPassword.setBounds(27, 359, 132, 17);
		lblPassword.setForeground(new Color(255, 51, 0));
		
		lblBackground = new JLabel("");
		ImageIcon background = new ImageIcon(LoginPanel.class.getResource(
				"/loginBackground.png"));
		lblBackground.setBounds(0, 0, 350, 500);
		lblBackground.setIcon(background);
		
		
		btnLogin = new JButton("Iniciar sesi�n");
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLogin.setForeground(new Color(0, 0, 0));
		btnLogin.setBackground(new Color(255, 51, 0));
		btnLogin.setBounds(169, 423, 146, 23);
		btnLogin.addActionListener(new LoginListener());
		
		btnExit = new JButton("Salir");
		btnExit.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnExit.setBackground(new Color(255, 51, 0));
		btnExit.setForeground(new Color(0, 0, 0));
		btnExit.setBounds(27, 423, 132, 23);
		btnExit.addActionListener(new SharedListeners());
	}
	
	
	public LoginPanel () {
		setBackground(Color.black);
		setLayout(null);
		
		add( txtFUsuario );
		add( txtFPassword );
		
		add( lblPassword );
		add( lblUsername );
		
		add( btnExit );
		add( btnLogin );
		
		add( lblBackground );
	}
	
	public static String getUserName () {
		return txtFUsuario.getText();
	}
	
	public static String getPassword () {
		return txtFPassword.getText();
	}

	public static void cleanTxtPassword() {
		txtFPassword.setText("");
	}

	public static void cleanTxtUsuario() {
		txtFUsuario.setText("");
	}
}