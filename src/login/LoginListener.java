package login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import adminWindows.AdminWindow;
import dataBaseMethods.Connectivity;

public class LoginListener implements ActionListener{
	public static AdminWindow adminWindow;

	public void actionPerformed(ActionEvent e) {
		String fontOfTheEvent = e.getActionCommand(), username = LoginPanel.getUserName();
		
		if (fontOfTheEvent.equals("Salir")) 
			System.exit(0);
		else if (fontOfTheEvent.equals("Iniciar sesi�n")) 
			if ( LoginMethods.checkUserAndPassword( username, LoginPanel.getPassword() ) ) {
				
				if ( Connectivity.SQLServerDriversChecking()	
						&& 		Connectivity.dataBaseConnectivityChecking())
					LoginMethods.openSessionWindow( username );
				else 
					JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos.", "Error",
							JOptionPane.ERROR_MESSAGE);
			
			} else
				JOptionPane.showMessageDialog(null, "Nombre de usuario o clave incorrecta.",
						"Error", JOptionPane.ERROR_MESSAGE);
	}
}