package mainMenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import adminWindows.AdminWindow;
import clientsButtonPanel.ClientsButtonPanel;
import guardsPanel.GuardsButtonPanel;
import mainWindow.MainWindow;
import servicesButtonPanel.ServicesButtonPanel;
import sharedClasses.SharedMethods;


public class MainMenuListener implements ActionListener{
	
	public void actionPerformed(ActionEvent e) {
		String fontOfTheEvent = e.getActionCommand();
		
		MainWindow.mainFrame.remove(AdminWindow.buttonPanel);
		
		 if (fontOfTheEvent.equals("Guardias")) 
			AdminWindow.buttonPanel = new GuardsButtonPanel();
		 else  if (fontOfTheEvent.equals("Clientes")) 
			 AdminWindow.buttonPanel = new ClientsButtonPanel();
		 else  if (fontOfTheEvent.equals("Servicios")) 
			 AdminWindow.buttonPanel = new ServicesButtonPanel();
		 
		 MainWindow.mainFrame.add(AdminWindow.buttonPanel);
		 
		 SharedMethods.refreshPanels();
	}
}
