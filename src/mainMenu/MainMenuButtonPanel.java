package mainMenu;

import javax.swing.JButton;
import javax.swing.JPanel;

import sharedClasses.DefaultComponentsConfiguration;

public class MainMenuButtonPanel extends JPanel {
	private static JButton btnGuards, btnClients, btnServices;
	
	static {
		btnGuards = new JButton("Guardias");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnGuards);
		btnGuards.setBounds(20, 140, 132, 23);
		btnGuards.addActionListener(new MainMenuListener());
		
		btnClients  = new JButton("Clientes");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnClients);
		btnClients.setBounds(20, 170, 132, 23);
		btnClients.addActionListener(new MainMenuListener());
		
		btnServices = new JButton("Servicios");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnServices);
		btnServices.setBounds(20, 200, 132, 23);
		btnServices.addActionListener(new MainMenuListener());
	}

	public MainMenuButtonPanel() {
		setLayout(null);
		setOpaque(false);
		setBounds(0, 0, 650, 500);
		
		add( btnGuards );
		add( btnClients );
		add( btnServices );
	}
}
