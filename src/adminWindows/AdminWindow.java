package adminWindows;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import mainMenu.MainMenuButtonPanel;
import sharedClasses.SessionBackgroundPanel;

public class AdminWindow extends JFrame{
	public static JPanel buttonPanel = new MainMenuButtonPanel();
	public static JPanel workPanel = new JPanel();
	
	public AdminWindow () {
		ImageIcon icon = new ImageIcon(AdminWindow.class.getResource("/icon.png"));
	      setIconImage(icon.getImage());
		
		setSize(650, 500);
		setUndecorated(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		add( workPanel );
		add( buttonPanel );
		add( new SessionBackgroundPanel() );
	}
}
