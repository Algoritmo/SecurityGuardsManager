package adminWindows;

import javax.swing.JPanel;

import mainMenu.MainMenuButtonPanel;
import sharedClasses.SessionBackgroundPanel;

public class AdminMainPanel extends JPanel{
	
	public static JPanel buttonPanel = new MainMenuButtonPanel();
	
	public AdminMainPanel () {
		setBounds(0, 0, 650, 500);
		setLayout(null);
		setOpaque(false);
		
		add(buttonPanel);
		add(new SessionBackgroundPanel());
	}
}
