package servicesButtonPanel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JPanel;

import mainMenu.MainMenuListener;
import sharedClasses.DefaultComponentsConfiguration;
import sharedClasses.SharedComponents;

public class ServicesButtonPanel extends JPanel {
	private static JButton btnSearch, btnNewContract, btnRemoveContract, btnListContracts,
								btnMainMenu;
	
	static {
		btnMainMenu = SharedComponents.btnMainMenu();
		
		btnSearch = new JButton("Buscar");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnSearch);
		btnSearch.setBounds(20, 140, 132, 23);
		btnSearch.addActionListener(new MainMenuListener());
			
		btnRemoveContract = new JButton("Finalizar contrato");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnRemoveContract);
		btnRemoveContract.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnRemoveContract.setBounds(19, 170, 134, 23);
		btnRemoveContract.addActionListener(new MainMenuListener());
			
		btnNewContract = new JButton("Nuevo contrato");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnNewContract);
		btnNewContract.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewContract.setBounds(20, 200, 132, 23);
		btnNewContract.addActionListener(new MainMenuListener());
		
		btnListContracts = new JButton("Listar contratos");
		DefaultComponentsConfiguration.setDefaultButtonConfiguration(btnListContracts);
		btnListContracts.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnListContracts.setBounds(20, 230, 132, 23);
		btnListContracts.addActionListener(new MainMenuListener());
	}
	
	public ServicesButtonPanel() {
		setOpaque(false);
		setLayout(null);
		setBounds(0, 0, 650, 500);
		
		add( btnListContracts );
		add( btnNewContract );
		add( btnRemoveContract );
		add( btnSearch );
		
		add( btnMainMenu );
	}
}
