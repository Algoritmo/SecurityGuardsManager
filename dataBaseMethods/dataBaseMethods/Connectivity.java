package dataBaseMethods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connectivity {
	public final static String URL =  "jdbc:sqlserver://localhost:1433;"
			+ "databaseName=GuardsDatabase;user=iris;password=clave123;";
	
	public static boolean SQLServerDriversChecking() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			return true;
		} catch (ClassNotFoundException e) {
			System.out.println(e);
			return false;
		}
	}
	
	
	public static boolean dataBaseConnectivityChecking() {
		try {
			Connection testingConnection = DriverManager.getConnection(URL);
			testingConnection.close();
			return true;
		} catch (SQLException e) {
			System.out.println(e);
			return false;
		}
	}
}
