package dataBaseMethods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import sharedClasses.Guard;

public class InsertMethods {
	
	public static void insertInto ( String tableName, String columns, String values) {
		try {
			Connection connection = DriverManager.getConnection(Connectivity.URL);
			Statement statement = connection.createStatement();
			String instruction = "INSERT INTO " + tableName + " ( " +  columns + " ) VALUES ( '" + values + "' );" ;
			
			statement.executeUpdate (instruction) ;
			
			connection.close();
		} catch (SQLException e) {
		}
	}
	
	public static void insertMainMartialArt( String ci, String martialArt ) {
		try {
			Connection connection = DriverManager.getConnection( Connectivity.URL );
			Statement statement = connection.createStatement();
			String instruction = "INSERT INTO GuardMainMartialArt (ci, martialArt) "
							+ "VALUES ('" + ci + "', '" + martialArt + "');" ;
			
			statement.executeUpdate( instruction );
			
			connection.close();
		} catch (SQLException e) {
		}
		
		
	}
	
	public static String convertStringArrayListToValuesString (ArrayList<String> arrayList) {
		String converted = "";
		
		for (int i = 0; i < arrayList.size(); i++) {
			converted += arrayList.get(i);
			if ( i		!=	arrayList.size() - 1 )
				converted += "', '";
		}
		
		return converted;
	}
	
	public static void  insertGuard (Guard guard) {
			String instruction = "INSERT INTO Guards (ci, firstName,";
			
			if (guard.secondName != null)		
				instruction += " secondName,";
				
			instruction +=	 " firstSurname, secondSurname, street, houseNumber, drivingLicense, weaponCarrying, "
					+ "workSchedule, category) VALUES ( '" + guard.ci + "', '" + guard.firstName +  "', '";
			
			if (guard.secondName != null)		
						instruction += guard.secondName + "', '";
			
			instruction += guard.firstSurname + "', '" + guard.secondSurname + "', '" + guard.street + "', '"
					+ guard.houseNumber + "', '" + guard.drivingLicense + "', '" + guard.weaponCarrying
					+ "', '" + guard.workSchedule + "', '" + guard.category + "' );" ;
			try {
				Connection connection = DriverManager.getConnection(Connectivity.URL);
				Statement statement = connection.createStatement();
				statement.executeUpdate (instruction) ;
				connection.close();
			} catch (SQLException e) {
			}
	}
	
	public static void insertSecondaryMartialArt (String ci, String martialArt) {
		try {
			Connection connection = DriverManager.getConnection( Connectivity.URL );
			String instruction = "INSERT INTO GuardSecondaryMartialArts (ci, martialArt) "
							+ "VALUES ('" + ci + "', '" + martialArt + "');";
			Statement statement = connection.createStatement();
			
			statement.executeUpdate( instruction );
			
			connection.close();
		} catch (SQLException e) {
		}
	}
}
