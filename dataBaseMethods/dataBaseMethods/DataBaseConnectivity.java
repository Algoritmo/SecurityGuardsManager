package dataBaseMethods;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataBaseConnectivity {

	public static boolean SQLServerDriversChecking() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			return true;
		} catch (ClassNotFoundException e) {
			System.out.println(e);
			return false;
		}
	}
	
	public static boolean dataBaseConnectivityChecking() {
		Connection testingConnection;
		String dataBaseName="GuardsDatabase";
		String url= "jdbc:sqlserver://localhost:1433;databaseName=" + dataBaseName +
				";user=iris;password=clave123;";
		try {
			testingConnection = DriverManager.getConnection(url);
			testingConnection.close();
			return true;
		} catch (SQLException e) {
			System.out.println(e);
			return false;
		}
	}
	
}
