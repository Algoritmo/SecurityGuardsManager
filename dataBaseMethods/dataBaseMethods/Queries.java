package dataBaseMethods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Queries {
	public static ArrayList getMartialArts () {
		ArrayList <String> martialArts = new ArrayList<String>(); 
		
		try {
			Connection connection = DriverManager.getConnection( Connectivity.URL );
			String instruction = "SELECT * FROM MartialArts;";
			PreparedStatement query = connection.prepareStatement(instruction);
			ResultSet result = query.executeQuery();
			
			martialArts.add("Seleccionar");
			while( result.next() ) {
				martialArts.add(result.getString(1));
			}
			
			connection.close();
		} catch (SQLException e) {
		} 
		return martialArts;
	}

	public static ArrayList<String> getGuardsData () {
		ArrayList<String> guardsData = new ArrayList<String>();;
		
		try {
			Connection connection =  DriverManager.getConnection( Connectivity.URL );
			Statement statement = connection.createStatement();
			String instruction = "SELECT firstSurname, secondSurname, firstName, secondName, ci,"
					+ "category  FROM Guards;";
			ResultSet result = statement.executeQuery( instruction );
			
			while ( result.next() )
				guardsData.add( result.getString(1) + "\t" + result.getString(2) + "\t" + result.getString(3) 
							+ "\t" + result.getString(4) + "\t" + result.getString(5) + "\t" + result.getString(6) 
							+ " ");
			
			connection.close();
		} catch (SQLException e) {
		}
		return guardsData;
	}
	
	public  static String [] getBasicGuardData ( String ci ) {
		String [] guardData = new String [7];
		
		try {
			Connection connection = DriverManager.getConnection( Connectivity.URL );
			
			Statement statement = connection.createStatement();
			String instruction = "SELECT firstName, secondName, firstSurname,"
					+ "secondSurname, category, street, houseNumber"
					+ " FROM Guards WHERE ci = '" + ci + "';";
			
			ResultSet result = statement.executeQuery( instruction );
			
			result.next() ;
			for ( int i = 1; i < 8; i++) 
				guardData [ i - 1 ] = result.getString( i );
			
			connection.close();
		} catch ( SQLException e ) {
		}
		return guardData;
	}
	
//	public static String [] getGuardPhone ( String ci ) {
//		String [] phones;
//		
//		
//		
//		return phones;
//	}
//	
//	public static String getGuardMainMartialArt ( String ci ) {
//		String mainMartialArt = "";
//		
//		
//		return mainMartialArt;
//	}
//	
//	public static String [] getGuardSecondaryMartialArts ( String ci ) {
//		String [] secondaryMartialArts;
//		
//		
//		
//		return secondaryMartialArts;
//	}

}
