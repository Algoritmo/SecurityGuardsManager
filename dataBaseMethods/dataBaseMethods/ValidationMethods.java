package dataBaseMethods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ValidationMethods {
	public static boolean guardAlreadyExists (String ci) {
		boolean exists = false;
		
		try {
			Connection connection = DriverManager.getConnection( Connectivity.URL );
			
			String instruction = "SELECT ci FROM Guards WHERE ci = '" + ci + "';";
			PreparedStatement query = connection.prepareStatement( instruction );
			ResultSet result = query.executeQuery();
			
			if ( result.next() )
				exists = ci.equals( result.getString( 1 ) );
			
			connection.close();
		} catch (SQLException e) {
		}
		return exists;
	}
}
