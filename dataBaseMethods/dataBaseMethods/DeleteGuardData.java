package dataBaseMethods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DeleteGuardData {
	public static void deleteAll ( String ci) {
		deletePhoneNumbers( ci );
		deleteMainMartialArt( ci );
		deleteSecondaryMartialArts( ci );
		deleteMainData( ci );
	}
	
	public static void deletePhoneNumbers ( String ci ) {
		try {
			Connection connection = DriverManager.getConnection( Connectivity.URL );
			Statement statement = connection.createStatement();
			String instruction = "DELETE FROM GuardsPhone "
					+ "WHERE ci = '" + ci + "';";
			
			statement.executeUpdate( instruction );
			
			connection.close();
		} catch (SQLException e) {
		}
	}
	
	public static void deleteMainMartialArt ( String ci) {
		try {
			Connection connection = DriverManager.getConnection( Connectivity.URL );
			Statement statement = connection.createStatement();
			String instruction = "DELETE FROM GuardMainMartialArt "
					+ "WHERE ci = '" + ci + "';";
			
			statement.executeUpdate( instruction );
			
			connection.close();
		} catch (SQLException e) {
		}
	}
	
	public static void deleteSecondaryMartialArts ( String ci ) {
		try {
			Connection connection = DriverManager.getConnection( Connectivity.URL );
			Statement statement = connection.createStatement();
			String instruction = "DELETE FROM GuardSecondaryMartialArts "
					+ "WHERE ci = '" + ci + "';";
			
			statement.executeUpdate( instruction );
			
			connection.close();
		} catch (SQLException e) {
		}
	}
	
	public static void deleteMainData ( String ci ) {
		try {
			Connection connection = DriverManager.getConnection( Connectivity.URL );
			Statement statement = connection.createStatement();
			String instruction = "DELETE FROM Guards "
					+ "WHERE ci = '" + ci + "';";
			
			statement.executeUpdate( instruction );
			
			connection.close();
		} catch (SQLException e) {
		}
	}
}
